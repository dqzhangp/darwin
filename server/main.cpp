/*
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * Copyright (c) 1999-2008 Apple Inc.  All Rights Reserved.
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 *
 */
/*
    File:       main.cpp

    Contains:   main function to drive streaming server.


*/

#include <stdio.h>
#include <stdlib.h>
#include "SafeStdLib.h"

#include <sys/time.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/resource.h>
#include <sys/wait.h>

#include "FilePrefsSource.h"
#include "RunServer.h"
#include "QTSServer.h"

static int sSigIntCount = 0;
static int sSigTermCount = 0;
static pid_t sChildPID = 0;

void usage()
{
    const char *usage_name = PACKAGE;

    qtss_printf("%s/%s ( Build/%s; Platform/%s; %s)\nBuilt on: %s\n",QTSServerInterface::GetServerName().Ptr,
                QTSServerInterface::GetServerVersion().Ptr,
                QTSServerInterface::GetServerBuild().Ptr,
                QTSServerInterface::GetServerPlatform().Ptr,
                QTSServerInterface::GetServerComment().Ptr,
                QTSServerInterface::GetServerBuildDate().Ptr);
    qtss_printf("usage: %s [ -d | -p port | -v | -c /myconfigpath.xml | -S numseconds | -I | -h ]\n", usage_name);
    qtss_printf("-d: Run in the foreground\n");
    qtss_printf("-D: Display performance data\n");
    qtss_printf("-p XXX: Specify the default RTSP listening port of the server\n");
    qtss_printf("-c /myconfigpath.xml: Specify a config file\n");
    qtss_printf("-S n: Display server stats in the console every \"n\" seconds\n");
    qtss_printf("-I: Start the server in the idle state\n");
    qtss_printf("-h: Prints usage\n");
}

Bool16 sendtochild(int sig, pid_t myPID)
{
    if (sChildPID != 0 && sChildPID != myPID) {// this is the parent, Send signal to child
        ::kill(sChildPID, sig);
        return true;
    }

    return false;
}

void sigcatcher(int sig, int /*sinfo*/, struct sigcontext* /*sctxt*/)
{
#if DEBUG
    qtss_printf("Signal %d caught\n", sig);
#endif
    pid_t myPID = getpid();
        
    //Try to shut down gracefully the first time, shutdown forcefully the next time
    if (sig == SIGINT) {// kill the child only
        if (sendtochild(sig,myPID)) {
            return;// ok we're done 
        } else {
            //
            // Tell the server that there has been a SigInt, the main thread will start
            // the shutdown process because of this. The parent and child processes will quit.
            if (sSigIntCount == 0)
                QTSServerInterface::GetServer()->SetSigInt();
            sSigIntCount++;
        }
    }
 
    if (sig == SIGTERM || sig == SIGQUIT) {// kill child then quit
        if (sendtochild(sig,myPID)) {
            return;// ok we're done 
        } else {
            // Tell the server that there has been a SigTerm, the main thread will start
            // the shutdown process because of this only the child will quit
            if (sSigTermCount == 0)
                QTSServerInterface::GetServer()->SetSigTerm();
            sSigTermCount++;
        }
    }
}

Bool16 RunInForeground()
{
    (void) setvbuf(stdout, NULL, _IOLBF, 0);
    OSThread::WrapSleep(true);
    
    return true;
}

Bool16 RestartServer(char* theXMLFilePath)
{
    Bool16 autoRestart = true;
    XMLPrefsParser theXMLParser(theXMLFilePath);
    theXMLParser.Parse();
 
    ContainerRef server = theXMLParser.GetRefForServer ();
    ContainerRef pref = theXMLParser.GetPrefRefByName (server, "auto_restart");
    char* autoStartSetting = NULL;
 
    if (pref != NULL)
        autoStartSetting = theXMLParser.GetPrefValueByRef (pref, 0, NULL, NULL);
  
    if ((autoStartSetting != NULL) && (::strcmp(autoStartSetting, "false") == 0))
        autoRestart = false;
  
    return autoRestart;
}

#define MAX_NOFILE 4096

int main (int argc, char * argv[]) 
{
    extern char* optarg;

    // on write, don't send signal for SIGPIPE, just set errno to EPIPE
    // and return -1
    //signal is a deprecated and potentially dangerous function
    //(void) ::signal(SIGPIPE, SIG_IGN);
    struct sigaction act;
    
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = (void(*)(int))&sigcatcher;

    (void)::sigaction(SIGPIPE, &act, NULL);
    (void)::sigaction(SIGHUP, &act, NULL);
    (void)::sigaction(SIGINT, &act, NULL);
    (void)::sigaction(SIGTERM, &act, NULL);
    (void)::sigaction(SIGQUIT, &act, NULL);
    (void)::sigaction(SIGALRM, &act, NULL);

    //grow our pool of file descriptors to the max!
    struct rlimit rl;
    
    // set it to the absolute maximum that the operating system allows - have to be superuser to do this
    rl.rlim_cur = MAX_NOFILE;
    rl.rlim_max = MAX_NOFILE;
    setrlimit (RLIMIT_NOFILE, &rl);

#if 0 // testing
    getrlimit(RLIMIT_NOFILE,  &rl);
    printf("current open file limit =%"_U32BITARG_"\n", (UInt32) rl.rlim_cur);
    printf("current open file max =%"_U32BITARG_"\n", (UInt32) rl.rlim_max);
#endif

    //First thing to do is to read command-line arguments.
    int ch;
    int thePort = 0; //port can be set on the command line
    int statsUpdateInterval = 0;
    QTSS_ServerState theInitialState = qtssRunningState;
    
    Bool16 dontFork = false;
    Bool16 theXMLPrefsExist = true;
    UInt32 debugLevel = 0;
    UInt32 debugOptions = kRunServerDebug_Off;
    static char* sDefaultConfigFilePath = (char *)DEFAULTPATHS_ETC_DIR_OLD "darwin.conf";
    static char* sDefaultXMLFilePath = (char *)DEFAULTPATHS_ETC_DIR "darwin.conf";

    char* theConfigFilePath = sDefaultConfigFilePath;
    char* theXMLFilePath = sDefaultXMLFilePath;
    while ((ch = getopt(argc,argv, "vdfxp:DZ:c:o:S:Ih")) != EOF) {// opt: means requires option arg
        switch(ch) {
        case 'v':
            usage();
            ::exit(0);  
        case 'd':
            dontFork = RunInForeground();
                
            break;                
        case 'D':
            dontFork = RunInForeground();

            debugOptions |= kRunServerDebugDisplay_On;
                
            if (debugLevel == 0)
                debugLevel = 1;
                    
            if (statsUpdateInterval == 0)
                statsUpdateInterval = 3;
                    
            break;            
        case 'Z':
            // this means we didn't declare getopt options correctly or there is a bug in getopt.
            Assert(optarg != NULL);
            debugLevel = (UInt32) ::atoi(optarg);        
            break;
        case 'f':
            theXMLFilePath  = (char *)DEFAULTPATHS_ETC_DIR "streamingserver.xml";
            break;
        case 'p':
            // this means we didn't declare getopt options correctly or there is a bug in getopt.
            Assert(optarg != NULL);
            thePort = ::atoi(optarg);
            break;
        case 'S':
            dontFork = RunInForeground();
            // this means we didn't declare getopt options correctly or there is a bug in getopt.
            Assert(optarg != NULL);
            statsUpdateInterval = ::atoi(optarg);
            break;
        case 'c':
            // this means we didn't declare getopt options correctly or there is a bug in getopt.
            Assert(optarg != NULL);
            theXMLFilePath = optarg;
            break;
        case 'o':
            // this means we didn't declare getopt options correctly or there is a bug in getopt.
            Assert(optarg != NULL);
            theConfigFilePath = optarg;
            break;
        case 'x':
            theXMLPrefsExist = false; // Force us to generate a new XML prefs file TODO: depricated
            theInitialState = qtssShuttingDownState;
            dontFork = true;
            break;
        case 'I':
            theInitialState = qtssIdleState;
            break;
        case 'h':
            usage();
            ::exit(0);
        default:
            break;
        }
    }

    // Check port
    if (thePort < 0 || thePort > 65535) { 
        qtss_printf("Invalid port value = %d max value = 65535\n",thePort);
        exit (-1);
    }

    XMLPrefsParser theXMLParser(theXMLFilePath);
    
    //
    // Check to see if the XML file exists as a directory. If it does,
    // just bail because we do not want to overwrite a directory
    if (theXMLParser.DoesFileExistAsDirectory()) {
        qtss_printf("Directory located at location where streaming server prefs file should be.\n");
        exit(-1);
    }

    //
    // Check to see if we can write to the file
    if (!theXMLParser.CanWriteFile()) {
        qtss_printf("Cannot write to the streaming server prefs file.\n");
        exit(-1);
    }

    // If we aren't forced to create a new XML prefs file, whether
    // we do or not depends solely on whether the XML prefs file exists currently.
    if (theXMLPrefsExist)
        theXMLPrefsExist = theXMLParser.DoesFileExist();
    
    if (!theXMLPrefsExist) {
        // The XML prefs file doesn't exist, so let's create an old-style
        // prefs source in order to generate a fresh XML prefs file.
        if (theConfigFilePath != NULL) {
            FilePrefsSource* filePrefsSource = new FilePrefsSource(true); // Allow dups
            
            if ( filePrefsSource->InitFromConfigFile(theConfigFilePath) ) {
                qtss_printf("Generating a new prefs file at %s\n", theXMLFilePath);
            }
        }
    }

    //
    // Parse the configs from the XML file
    int xmlParseErr = theXMLParser.Parse();
    if (xmlParseErr) {
        qtss_printf("Fatal Error: Could not load configuration file at %s. (%d)\n",
                    theXMLFilePath, OSThread::GetErrno());
        ::exit(-1);
    }
    
    //Unless the command line option is set, fork & daemonize the process at this point
    if (!dontFork) {
        if (daemon(0,0) != 0) {
#if DEBUG
            qtss_printf("Failed to daemonize process. Error = %d\n", OSThread::GetErrno());
#endif
            exit(-1);
        }
    }
    
    //Construct a Prefs Source object to get server text messages
    FilePrefsSource theMessagesSource;
    theMessagesSource.InitFromConfigFile("qtssmessages.txt");
    

    int status = 0;
    int pid = 0;
    pid_t processID = 0;
 
    if (!dontFork) {// if (fork) 
        //loop until the server exits normally. If the server doesn't exit
        //normally, then restart it.
        // normal exit means the following
        // the child quit 
        do {// fork at least once but stop on the status conditions returned by wait or if autoStart pref is false
            processID = fork();
            Assert(processID >= 0);
            if (processID > 0) {// this is the parent and we have a child
                sChildPID = processID;
                status = 0;
                while (status == 0) {//loop on wait until status is != 0;
                    pid =::wait(&status);
                    SInt8 exitStatus = (SInt8) WEXITSTATUS(status);
                    // child exited with status -2 restart or -1 don't restart 
                    if (WIFEXITED(status) && pid > 0 && status != 0) {
                        //qtss_printf("child exited with status=%d\n", exitStatus);
                        if ( exitStatus == -1) {// child couldn't run don't try again
                            qtss_printf("child exited with -1 fatal error"
                                        " so parent is exiting too.\n");
                            exit (EXIT_FAILURE); 
                        }
                        break; // restart the child
                    }
                    // child exited on an unhandled signal (maybe a bus error or seg fault)
                    if (WIFSIGNALED(status)) {
                        //qtss_printf("child was signalled\n");
                        break; // restart the child
                    }

                   
                    if (pid == -1 && status == 0) {// parent woken up by a handled signal
                        //qtss_printf("handled signal continue waiting\n");
                        continue;
                    }
                    
                    if (pid > 0 && status == 0) {
                        //qtss_printf("child exited cleanly so parent is exiting\n");
                        exit(EXIT_SUCCESS);                  
                    }
                 
                    //qtss_printf("child died for unknown reasons parent is exiting\n");
                    exit (EXIT_FAILURE);
                }
            } else if (processID == 0) // must be the child
                break;
            else
                exit(EXIT_FAILURE);
             
            //eek. If you auto-restart too fast, you might start the new one before the OS has
            //cleaned up from the old one, resulting in startup errors when you create the new
            //one. Waiting for a second seems to work
            sleep(1);
        } while (RestartServer(theXMLFilePath)); // fork again based on pref if server dies
        if (processID != 0) //the parent is quitting
            exit(EXIT_SUCCESS);   
    }
    sChildPID = 0;
    //we have to do this again for the child process, because sigaction states
    //do not span multiple processes.
    (void)::sigaction (SIGPIPE, &act, NULL);
    (void)::sigaction (SIGHUP, &act, NULL);
    (void)::sigaction (SIGINT, &act, NULL);
    (void)::sigaction (SIGTERM, &act, NULL);
    (void)::sigaction (SIGQUIT, &act, NULL);

    //This function starts, runs, and shuts down the server
    if (::StartServer(&theXMLParser, &theMessagesSource, thePort, statsUpdateInterval,
                      theInitialState, dontFork, debugLevel, debugOptions) != qtssFatalErrorState) {
        ::RunServer();
        exit (EXIT_SUCCESS);
    } else
        exit(-1); //Cant start server don't try again
}

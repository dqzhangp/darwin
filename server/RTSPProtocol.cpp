/*
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * Copyright (c) 1999-2008 Apple Inc.  All Rights Reserved.
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 *
 */
/*
    File:       RTSPProtocol.cpp

    Contains:   Implementation of class defined in RTSPProtocol.h

*/

#include <ctype.h>
#include "RTSPProtocol.h"

StrPtrLen RTSPProtocol::sRetrProtName((char *)"our-retransmit");

StrPtrLen RTSPProtocol::sMethods[] =
{
    StrPtrLen((char *)"DESCRIBE"),
    StrPtrLen((char *)"SETUP"),
    StrPtrLen((char *)"TEARDOWN"),
    StrPtrLen((char *)"PLAY"),
    StrPtrLen((char *)"PAUSE"),
    StrPtrLen((char *)"OPTIONS"),
    StrPtrLen((char *)"ANNOUNCE"),
    StrPtrLen((char *)"GET_PARAMETER"),
    StrPtrLen((char *)"SET_PARAMETER"),
    StrPtrLen((char *)"REDIRECT"),
    StrPtrLen((char *)"RECORD")
};

QTSS_RTSPMethod
RTSPProtocol::GetMethod(const StrPtrLen &inMethodStr)
{
    //chances are this is one of our selected "VIP" methods. so check for this.
    QTSS_RTSPMethod theMethod = qtssIllegalMethod;
    
    switch(*inMethodStr.Ptr) {
    case 'S':
    case 's':
        theMethod = qtssSetupMethod;
        break;
    case 'D':
    case 'd':
        theMethod = qtssDescribeMethod;
        break;
    case 'T':
    case 't':
        theMethod = qtssTeardownMethod;
        break;
    case 'O':
    case 'o':
        theMethod = qtssOptionsMethod;
        break;
    case 'A':
    case 'a':
        theMethod = qtssAnnounceMethod;
        break;
    case 'G':
    case 'g':
        theMethod = qtssGetParameterMethod;
        break;
    }

    if ((theMethod != qtssIllegalMethod) &&
        (inMethodStr.EqualIgnoreCase(sMethods[theMethod].Ptr, sMethods[theMethod].Len)))
        return theMethod;

    for (SInt32 x = qtssNumVIPMethods; x < qtssIllegalMethod; x++)
        if (inMethodStr.EqualIgnoreCase(sMethods[x].Ptr, sMethods[x].Len))
            return x;
    return qtssIllegalMethod;
}

StrPtrLen RTSPProtocol::sHeaders[] =
{
    StrPtrLen((char *)"Accept"),
    StrPtrLen((char *)"Cseq"),
    StrPtrLen((char *)"User-Agent"),
    StrPtrLen((char *)"Transport"),
    StrPtrLen((char *)"Session"),
    StrPtrLen((char *)"Range"),

    StrPtrLen((char *)"Accept-Encoding"),
    StrPtrLen((char *)"Accept-Language"),
    StrPtrLen((char *)"Authorization"),
    StrPtrLen((char *)"Bandwidth"),
    StrPtrLen((char *)"Blocksize"),
    StrPtrLen((char *)"Cache-Control"),
    StrPtrLen((char *)"Conference"),
    StrPtrLen((char *)"Connection"),
    StrPtrLen((char *)"Content-Base"),
    StrPtrLen((char *)"Content-Encoding"),
    StrPtrLen((char *)"Content-Language"),
    StrPtrLen((char *)"Content-length"),
    StrPtrLen((char *)"Content-Location"),
    StrPtrLen((char *)"Content-Type"),
    StrPtrLen((char *)"Date"),
    StrPtrLen((char *)"Expires"),
    StrPtrLen((char *)"From"),
    StrPtrLen((char *)"Host"),
    StrPtrLen((char *)"If-Match"),
    StrPtrLen((char *)"If-Modified-Since"),
    StrPtrLen((char *)"Last-Modified"),
    StrPtrLen((char *)"Location"),
    StrPtrLen((char *)"Proxy-Authenticate"),
    StrPtrLen((char *)"Proxy-Require"),
    StrPtrLen((char *)"Referer"),
    StrPtrLen((char *)"Retry-After"),
    StrPtrLen((char *)"Require"),
    StrPtrLen((char *)"RTP-Info"),
    StrPtrLen((char *)"Scale"),
    StrPtrLen((char *)"Speed"),
    StrPtrLen((char *)"Timestamp"),
    StrPtrLen((char *)"Vary"),
    StrPtrLen((char *)"Via"),
    StrPtrLen((char *)"Allow"),
    StrPtrLen((char *)"Public"),
    StrPtrLen((char *)"Server"),
    StrPtrLen((char *)"Unsupported"),
    StrPtrLen((char *)"WWW-Authenticate"),
    StrPtrLen((char *)","),
    StrPtrLen((char *)"x-Retransmit"),
    StrPtrLen((char *)"x-Accept-Retransmit"),
    StrPtrLen((char *)"x-RTP-Meta-Info"),
    StrPtrLen((char *)"x-Transport-Options"),
    StrPtrLen((char *)"x-Packet-Range"),
    StrPtrLen((char *)"x-Prebuffer"),
    StrPtrLen((char *)"x-Dynamic-Rate"),
    StrPtrLen((char *)"x-Accept-Dynamic-Rate"),
    // DJM PROTOTYPE
    StrPtrLen((char *)"x-Random-Data-Size"),
    StrPtrLen((char *)"Timeinfo"),
};

QTSS_RTSPHeader RTSPProtocol::GetRequestHeader(const StrPtrLen &inHeaderStr)
{
    if (inHeaderStr.Len == 0)
        return qtssIllegalHeader;
    
    QTSS_RTSPHeader theHeader = qtssIllegalHeader;
    
    //chances are this is one of our selected "VIP" headers. so check for this.
    switch(*inHeaderStr.Ptr) {
    case 'C':
    case 'c':
        theHeader = qtssCSeqHeader;
        break;
    case 'S':
    case 's':
        theHeader = qtssSessionHeader;
        break;
    case 'U':
    case 'u':
        theHeader = qtssUserAgentHeader;
        break;
    case 'A':
    case 'a':
        theHeader = qtssAcceptHeader;
        break;
    case 'T':
    case 't':
        theHeader = qtssTransportHeader;
        break;
    case 'R':
    case 'r':
        theHeader = qtssRangeHeader;
        break;
    case 'X':
    case 'x':
        theHeader = qtssExtensionHeaders;
        break;
    }
    
    //
    // Check to see whether this is one of our extension headers. These
    // are very likely to appear in requests.
    if (theHeader == qtssExtensionHeaders) {
        for (SInt32 y = qtssExtensionHeaders; y < qtssNumHeaders; y++) {
            if (inHeaderStr.EqualIgnoreCase(sHeaders[y].Ptr, sHeaders[y].Len))
                return y;
        }
    }
    
    //
    // It's not one of our extension headers, check to see if this is one of
    // our normal VIP headers
    if ((theHeader != qtssIllegalHeader) &&
        (inHeaderStr.EqualIgnoreCase(sHeaders[theHeader].Ptr, sHeaders[theHeader].Len)))
        return theHeader;

    //
    //If this isn't one of our VIP headers, go through the remaining request headers, trying
    //to find the right one.
    for (SInt32 x = qtssNumVIPHeaders; x < qtssNumHeaders; x++) {
        if (inHeaderStr.EqualIgnoreCase(sHeaders[x].Ptr, sHeaders[x].Len))
            return x;
    }
    return qtssIllegalHeader;
}

StrPtrLen RTSPProtocol::sStatusCodeStrings[] =
{
    StrPtrLen((char *)"Continue"),                              //kContinue
    StrPtrLen((char *)"OK"),                                    //kSuccessOK
    StrPtrLen((char *)"Created"),                               //kSuccessCreated
    StrPtrLen((char *)"Accepted"),                              //kSuccessAccepted
    StrPtrLen((char *)"No Content"),                            //kSuccessNoContent
    StrPtrLen((char *)"Partial Content"),                       //kSuccessPartialContent
    StrPtrLen((char *)"Low on Storage Space"),                  //kSuccessLowOnStorage
    StrPtrLen((char *)"Multiple Choices"),                      //kMultipleChoices
    StrPtrLen((char *)"Moved Permanently"),                     //kRedirectPermMoved
    StrPtrLen((char *)"Found"),                                 //kRedirectTempMoved
    StrPtrLen((char *)"See Other"),                             //kRedirectSeeOther
    StrPtrLen((char *)"Not Modified"),                          //kRedirectNotModified
    StrPtrLen((char *)"Use Proxy"),                             //kUseProxy
    StrPtrLen((char *)"Bad Request"),                           //kClientBadRequest
    StrPtrLen((char *)"Unauthorized"),                          //kClientUnAuthorized
    StrPtrLen((char *)"Payment Required"),                      //kPaymentRequired
    StrPtrLen((char *)"Forbidden"),                             //kClientForbidden
    StrPtrLen((char *)"Not Found"),                             //kClientNotFound
    StrPtrLen((char *)"Method Not Allowed"),                    //kClientMethodNotAllowed
    StrPtrLen((char *)"Not Acceptable"),                        //kNotAcceptable
    StrPtrLen((char *)"Proxy Authentication Required"),         //kProxyAuthenticationRequired
    StrPtrLen((char *)"Request Time-out"),                      //kRequestTimeout
    StrPtrLen((char *)"Conflict"),                              //kClientConflict
    StrPtrLen((char *)"Gone"),                                  //kGone
    StrPtrLen((char *)"Length Required"),                       //kLengthRequired
    StrPtrLen((char *)"Precondition Failed"),                   //kPreconditionFailed
    StrPtrLen((char *)"Request Entity Too Large"),              //kRequestEntityTooLarge
    StrPtrLen((char *)"Request-URI Too Large"),                 //kRequestURITooLarge
    StrPtrLen((char *)"Unsupported Media Type"),                //kUnsupportedMediaType
    StrPtrLen((char *)"Parameter Not Understood"),              //kClientParameterNotUnderstood
    StrPtrLen((char *)"Conference Not Found"),                  //kClientConferenceNotFound
    StrPtrLen((char *)"Not Enough Bandwidth"),                  //kClientNotEnoughBandwidth
    StrPtrLen((char *)"Session Not Found"),                     //kClientSessionNotFound
    StrPtrLen((char *)"Method Not Valid in this State"),        //kClientMethodNotValidInState
    StrPtrLen((char *)"Header Field Not Valid For Resource"),   //kClientHeaderFieldNotValid
    StrPtrLen((char *)"Invalid Range"),                         //kClientInvalidRange
    StrPtrLen((char *)"Parameter Is Read-Only"),                //kClientReadOnlyParameter
    StrPtrLen((char *)"Aggregate Option Not Allowed"),          //kClientAggregateOptionNotAllowed
    StrPtrLen((char *)"Only Aggregate Option Allowed"),         //kClientAggregateOptionAllowed
    StrPtrLen((char *)"Unsupported Transport"),                 //kClientUnsupportedTransport
    StrPtrLen((char *)"Destination Unreachable"),               //kClientDestinationUnreachable
    StrPtrLen((char *)"Internal Server Error"),                 //kServerInternal
    StrPtrLen((char *)"Not Implemented"),                       //kServerNotImplemented
    StrPtrLen((char *)"Bad Gateway"),                           //kServerBadGateway
    StrPtrLen((char *)"Service Unavailable"),                   //kServerUnavailable
    StrPtrLen((char *)"Gateway Timeout"),                       //kServerGatewayTimeout
    StrPtrLen((char *)"RTSP Version not supported"),            //kRTSPVersionNotSupported
    StrPtrLen((char *)"Option Not Supported")                   //kServerOptionNotSupported
};

SInt32 RTSPProtocol::sStatusCodes[] =
{
    100,        //kContinue
    200,        //kSuccessOK
    201,        //kSuccessCreated
    202,        //kSuccessAccepted
    204,        //kSuccessNoContent
    206,        //kSuccessPartialContent
    250,        //kSuccessLowOnStorage
    300,        //kMultipleChoices
    301,        //kRedirectPermMoved
    302,        //kRedirectTempMoved
    303,        //kRedirectSeeOther
    304,        //kRedirectNotModified
    305,        //kUseProxy
    400,        //kClientBadRequest
    401,        //kClientUnAuthorized
    402,        //kPaymentRequired
    403,        //kClientForbidden
    404,        //kClientNotFound
    405,        //kClientMethodNotAllowed
    406,        //kNotAcceptable
    407,        //kProxyAuthenticationRequired
    408,        //kRequestTimeout
    409,        //kClientConflict
    410,        //kGone
    411,        //kLengthRequired
    412,        //kPreconditionFailed
    413,        //kRequestEntityTooLarge
    414,        //kRequestURITooLarge
    415,        //kUnsupportedMediaType
    451,        //kClientParameterNotUnderstood
    452,        //kClientConferenceNotFound
    453,        //kClientNotEnoughBandwidth
    454,        //kClientSessionNotFound
    455,        //kClientMethodNotValidInState
    456,        //kClientHeaderFieldNotValid
    457,        //kClientInvalidRange
    458,        //kClientReadOnlyParameter
    459,        //kClientAggregateOptionNotAllowed
    460,        //kClientAggregateOptionAllowed
    461,        //kClientUnsupportedTransport
    462,        //kClientDestinationUnreachable
    500,        //kServerInternal
    501,        //kServerNotImplemented
    502,        //kServerBadGateway
    503,        //kServerUnavailable
    504,        //kServerGatewayTimeout
    505,        //kRTSPVersionNotSupported
    551         //kServerOptionNotSupported
};

StrPtrLen RTSPProtocol::sStatusCodeAsStrings[] =
{
    StrPtrLen((char *)"100"),       //kContinue
    StrPtrLen((char *)"200"),       //kSuccessOK
    StrPtrLen((char *)"201"),       //kSuccessCreated
    StrPtrLen((char *)"202"),       //kSuccessAccepted
    StrPtrLen((char *)"204"),       //kSuccessNoContent
    StrPtrLen((char *)"206"),       //kSuccessPartialContent
    StrPtrLen((char *)"250"),       //kSuccessLowOnStorage
    StrPtrLen((char *)"300"),       //kMultipleChoices
    StrPtrLen((char *)"301"),       //kRedirectPermMoved
    StrPtrLen((char *)"302"),       //kRedirectTempMoved
    StrPtrLen((char *)"303"),       //kRedirectSeeOther
    StrPtrLen((char *)"304"),       //kRedirectNotModified
    StrPtrLen((char *)"305"),       //kUseProxy
    StrPtrLen((char *)"400"),       //kClientBadRequest
    StrPtrLen((char *)"401"),       //kClientUnAuthorized
    StrPtrLen((char *)"402"),       //kPaymentRequired
    StrPtrLen((char *)"403"),       //kClientForbidden
    StrPtrLen((char *)"404"),       //kClientNotFound
    StrPtrLen((char *)"405"),       //kClientMethodNotAllowed
    StrPtrLen((char *)"406"),       //kNotAcceptable
    StrPtrLen((char *)"407"),       //kProxyAuthenticationRequired
    StrPtrLen((char *)"408"),       //kRequestTimeout
    StrPtrLen((char *)"409"),       //kClientConflict
    StrPtrLen((char *)"410"),       //kGone
    StrPtrLen((char *)"411"),       //kLengthRequired
    StrPtrLen((char *)"412"),       //kPreconditionFailed
    StrPtrLen((char *)"413"),       //kRequestEntityTooLarge
    StrPtrLen((char *)"414"),       //kRequestURITooLarge
    StrPtrLen((char *)"415"),       //kUnsupportedMediaType
    StrPtrLen((char *)"451"),       //kClientParameterNotUnderstood
    StrPtrLen((char *)"452"),       //kClientConferenceNotFound
    StrPtrLen((char *)"453"),       //kClientNotEnoughBandwidth
    StrPtrLen((char *)"454"),       //kClientSessionNotFound
    StrPtrLen((char *)"455"),       //kClientMethodNotValidInState
    StrPtrLen((char *)"456"),       //kClientHeaderFieldNotValid
    StrPtrLen((char *)"457"),       //kClientInvalidRange
    StrPtrLen((char *)"458"),       //kClientReadOnlyParameter
    StrPtrLen((char *)"459"),       //kClientAggregateOptionNotAllowed
    StrPtrLen((char *)"460"),       //kClientAggregateOptionAllowed
    StrPtrLen((char *)"461"),       //kClientUnsupportedTransport
    StrPtrLen((char *)"462"),       //kClientDestinationUnreachable
    StrPtrLen((char *)"500"),       //kServerInternal
    StrPtrLen((char *)"501"),       //kServerNotImplemented
    StrPtrLen((char *)"502"),       //kServerBadGateway
    StrPtrLen((char *)"503"),       //kServerUnavailable
    StrPtrLen((char *)"504"),       //kServerGatewayTimeout
    StrPtrLen((char *)"505"),       //kRTSPVersionNotSupported
    StrPtrLen((char *)"551")        //kServerOptionNotSupported
};

StrPtrLen RTSPProtocol::sVersionString[] = 
{
    StrPtrLen((char *)"RTSP/1.0")
};

RTSPProtocol::RTSPVersion
RTSPProtocol::GetVersion(StrPtrLen &versionStr)
{
    if (versionStr.Len != 8)
        return kIllegalVersion;
    else
        return k10Version;
}

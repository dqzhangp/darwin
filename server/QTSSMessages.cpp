/*
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * Copyright (c) 1999-2008 Apple Inc.  All Rights Reserved.
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 *
 */
/*
    File:       QTSSMessages.cpp

    Contains:   Implementation of object defined in .h
    
    
    
*/

#include "QTSSMessages.h"
#include "OSMemory.h"

// see QTSS.h (QTSS_TextMessagesObject) for list of enums to map these strings

char* QTSSMessages::sMessagesKeyStrings[] =
{ /* index */
	/* 0 */ (char *)"qtssMsgNoMessage",
	/* 1 */ (char *)"qtssMsgNoURLInRequest",
	/* 2 */ (char *)"qtssMsgBadRTSPMethod",
	/* 3 */ (char *)"qtssMsgNoRTSPVersion",
	/* 4 */ (char *)"qtssMsgNoRTSPInURL",
	/* 5 */ (char *)"qtssMsgURLTooLong",
	/* 6 */ (char *)"qtssMsgURLInBadFormat",
	/* 7 */ (char *)"qtssMsgNoColonAfterHeader",
	/* 8 */ (char *)"qtssMsgNoEOLAfterHeader",
	/* 9 */ (char *)"qtssMsgRequestTooLong",
	/* 10*/ (char *)"qtssMsgNoModuleFolder",
	/* 11*/ (char *)"qtssMsgCouldntListen",
	/* 12*/ (char *)"qtssMsgInitFailed",
	/* 13*/ (char *)"qtssMsgNotConfiguredForIP",
	/* 14*/ (char *)"qtssMsgDefaultRTSPAddrUnavail",
	/* 15*/ (char *)"qtssMsgBadModule",
	/* 16*/ (char *)"qtssMsgRegFailed",
	/* 17*/ (char *)"qtssMsgRefusingConnections",
	/* 18*/ (char *)"qtssMsgTooManyClients",
	/* 19*/ (char *)"qtssMsgTooMuchThruput",
	/* 20*/ (char *)"qtssMsgNoSessionID",
	/* 21*/ (char *)"qtssMsgFileNameTooLong",
	/* 22*/ (char *)"qtssMsgNoClientPortInTransport",
	/* 23*/ (char *)"qtssMsgRTPPortMustBeEven",
	/* 24*/ (char *)"qtssMsgRTCPPortMustBeOneBigger",
	/* 25*/ (char *)"qtssMsgOutOfPorts",
	/* 26*/ (char *)"qtssMsgNoModuleForRequest",
	/* 27*/ (char *)"qtssMsgAltDestNotAllowed",
	/* 28*/ (char *)"qtssMsgCantSetupMulticast",
	/* 29*/ (char *)"qtssListenPortInUse",
	/* 30*/ (char *)"qtssListenPortAccessDenied",
	/* 31*/ (char *)"qtssListenPortError",
	/* 32*/ (char *)"qtssMsgBadBase64",
	/* 33*/ (char *)"qtssMsgSomePortsFailed",
	/* 34*/ (char *)"qtssMsgNoPortsSucceeded",
	/* 35*/ (char *)"qtssMsgCannotCreatePidFile",
	/* 36*/ (char *)"qtssMsgCannotSetRunUser",
	/* 37*/ (char *)"qtssMsgCannotSetRunGroup",
	/* 38*/ (char *)"qtssMsgNoSesIDOnDescribe",
	/* 39*/ (char *)"qtssServerPrefMissing",
	/* 40*/ (char *)"qtssServerPrefWrongType",
	/* 41*/ (char *)"qtssMsgCantWriteFile",
	/* 42*/ (char *)"qtssMsgSockBufSizesTooLarge",
	/* 43*/ (char *)"qtssMsgBadFormat",

	// module specific messages follow (these are dynamically numbered)
 
	/* 44*/ (char *)"QTSSvrControlModuleCantRegisterMachPort",
	/* 45*/ (char *)"QTSSvrControlModuleServerControlFatalErr",
	/* 46*/ (char *)"QTSSReflectorModuleCantBindReflectorSocket",
	/* 47*/ (char *)"QTSSReflectorModuleCantJoinMulticastGroup",
	/* 48*/ (char *)"QTSSFileModuleSeekToNonExistentTime",
	/* 49*/ (char *)"QTSSFileModuleNoSDPFileFound",
	/* 50*/ (char *)"QTSSFileModuleBadQTFile",
	/* 51*/ (char *)"QTSSFileModuleFileIsNotHinted",
	/* 52*/ (char *)"QTSSFileModuleExpectedDigitFilename",
	/* 53*/ (char *)"QTSSFileModuleTrackDoesntExist",
	/* 54*/ (char *)"QTSSReflectorModuleExpectedDigitFilename",
	/* 55*/ (char *)"QTSSSpamDefenseModuleTooManyConnections",
	/* 56*/ (char *)"QTSSReflectorModuleBadTrackID",
	/* 57*/ (char *)"QTSSAccessModuleBadAccessFileName",
	/* 58*/ (char *)"QTSSReflectorModuleNoRelaySources",
	/* 59*/ (char *)"QTSSReflectorModuleNoRelayDests",
	/* 60*/ (char *)"QTSSReflectorModuleNoRelayStreams",
	/* 61*/ (char *)"QTSSReflectorModuleNoRelayConfig",
	/* 62*/ (char *)"QTSSReflectorModuleDuplicateBroadcastStream",
	/* 63*/ (char *)"QTSSAccessModuleUsersFileNotFound",
	/* 64*/ (char *)"QTSSAccessModuleGroupsFileNotFound",
	/* 65*/ (char *)"QTSSAccessModuleBadUsersFile",
	/* 66*/ (char *)"QTSSAccessModuleBadGroupsFile",
	/* 67*/ (char *)"QTSSReflectorModuleAnnounceRequiresSDPSuffix",
	/* 68*/ (char *)"QTSSReflectorModuleAnnounceDisabled",
	/* 69*/ (char *)"QTSSReflectorModuleSDPPortMinimumPort",
	/* 70*/ (char *)"QTSSReflectorModuleSDPPortMaximumPort",
	/* 71*/ (char *)"QTSSReflectorModuleStaticPortsConflict",
	/* 72*/ (char *)"QTSSReflectorModuleStaticPortPrefsBadRange",
	/* 73*/ (char *)"QTSSRelayModulePrefParseError"
};

// see QTSS.h (QTSS_TextMessagesObject) for list of enums to map these strings

char* QTSSMessages::sMessages[] =
{
	/* 0 */ (char *)"%s%s",
	/* 1 */ (char *)"There was no URL contained in the following request: %s",
	/* 2 */ (char *)"The following RTSP method: %s, was not understood by the server",
	/* 3 */ (char *)"There is no RTSP version in the following request: %s",
	/* 4 */ (char *)"Server expected 'rtsp://' and instead received: %s",
	/* 5 */ (char *)"The following URL is too long to be processed by the server: %s",
	/* 6 */ (char *)"The following URL is not in proper URL format: %s",
	/* 7 */ (char *)"There was no colon after a header in the following request: %s",
	/* 8 */ (char *)"There was no EOL after a header in the following request: %s",
	/* 9 */ (char *)"That request is too long to be processed by the server.",
	/* 10*/ (char *)"No module folder exists.",
	/* 11*/ (char *)"Streaming Server couldn't listen on a specified RTSP port. Quitting.",
	/* 12*/ (char *)"The module %s failed to Initialize.",
	/* 13*/ (char *)"This machine is currently not configured for IP.",
	/* 14*/ (char *)"The specified RTSP listening IP address doesn't exist.",
	/* 15*/ (char *)"The module %s is not a compatible QTSS API module.",
	/* 16*/ (char *)"The module %s failed to Register.",
	/* 17*/ (char *)"Streaming Server is currently refusing new connections",
	/* 18*/ (char *)"Too many clients connected",
	/* 19*/ (char *)"Too much bandwidth being served",
	/* 20*/ (char *)"No active RTP session for that RTSP session ID",
	/* 21*/ (char *)"Specified file name is too long to be handled by the server.",
	/* 22*/ (char *)"No client port pair found in transport header",
	/* 23*/ (char *)"Reported client RTP port is not an even number",
	/* 24*/ (char *)"Reported client RTCP port is not one greater than RTP port",
	/* 25*/ (char *)"Streaming Server couldn't find any available UDP ports",
	/* 26*/ (char *)"There is no QTSS API module installed to process this request.",
	/* 27*/ (char *)"Not allowed to specify an alternate destination address",
	/* 28*/ (char *)"Can't setup multicast.",
	/* 29*/ (char *)"Another process is already using the following RTSP port: %s",
	/* 30*/ (char *)"You must be root to use the following RTSP port: %s",
	/* 31*/ (char *)"An error occurred when attempting to listen on the following RTSP port: %s",
	/* 32*/ (char *)"The base64 you just sent to the server is corrupt!",
	/* 33*/ (char *)"Streaming Server failed to listen on all requested RTSP port(s).",
	/* 34*/ (char *)"Streaming Server is not listening for RTSP on any ports.",
	/* 35*/ (char *)"Error creating pid file %s: %s",
	/* 36*/ (char *)"Error switching to user %s: %s",
	/* 37*/ (char *)"Error switching to group %s: %s",
	/* 38*/ (char *)"A DESCRIBE request cannot contain the Session header",
	/* 39*/ (char *)"The following pref, %s, wasn't found. Using a default value of: %s",
	/* 40*/ (char *)"The following pref, %s, has the wrong type. Using a default value of: %s",
	/* 41*/ (char *)"Couldn't re-write server prefs file",
	/* 42*/ (char *)"Couldn't set desired UDP receive socket buffer size. Using size of %s K",
	/* 43*/ (char *)"The request is incorrectly formatted.",

// module specific messages follow (these are dynamically numbered)

	/* 44*/ (char *)"Fatal error: Can't register Mach Ports.",
	/* 45*/ (char *)"A fatal error occcured while starting up server control module",
	/* 46*/ (char *)"Can't bind reflector sockets",
	/* 47*/ (char *)"Reflector sockets couldn't join multicast",
	/* 48*/ (char *)"Couldn't seek to specified time.",
	/* 49*/ (char *)"No SDP file found for the following URL: %s",
	/* 50*/ (char *)"The requested file is not a movie file.",
	/* 51*/ (char *)"Requested movie hasn't been hinted.",
	/* 52*/ (char *)"Expected a digit at the end of the following URL: %s",
	/* 53*/ (char *)"Specified trackID doesn't exist in the movie",
	/* 54*/ (char *)"Expected a digit at the end of the following URL: %s",
	/* 55*/ (char *)"Too many connections from your IP address!",
	/* 56*/ (char *)"TrackID doesn't match any trackID for this ReflectorSession",
	/* 57*/ (char *)"Invalid config value for qtaccessfilename: name contains %s. Now using default name:%s",
	/* 58*/ (char *)"The relay configuration file at: %s has no relay_source lines",
	/* 59*/ (char *)"Could not find any relay_destination lines for one of the relay_source lines of the relay configuration file",
	/* 60*/ (char *)"Could not find any input stream information for one of the relay_source lines of the relay configuration file",
	/* 61*/ (char *)"Found an empty relay configuration file at %s",
	/* 62*/ (char *)"A broadcast stream is already setup for this URL",
	/* 63*/ (char *)"No users file found at %s.",
	/* 64*/ (char *)"No groups file found at %s.",
	/* 65*/ (char *)"Unable to read the users file at %s. It may be corrupted.",
	/* 66*/ (char *)"Unable to read the groups file at %s. It may be corrupted.",
	/* 67*/ (char *)"The Announced file does not end with .sdp",
	/* 68*/ (char *)"The Announce feature is disabled. Your request is denied.",
	/* 69*/ (char *)"The SDP file's static port %s is less than the QTSSReflectorModule's minimum_static_sdp_port preference.",
	/* 70*/ (char *)"The SDP file's static port %s is greater than the QTSSReflectorModule's maximum_static_sdp_port preference.",
	/* 71*/ (char *)"The QTSSReflectorModule's minimum_static_sdp_port and maximum_static_sdp_port preferences conflict with the client and dynamic broadcast port range= %s to %s.",
	/* 72*/ (char *)"The QTSSReflectorModule's minimum_static_sdp_port and maximum_static_sdp_port preferences define an invalid range (min=%s > max=%s).",
	/* 73*/ (char *)"The QTSSRelayModule encountered an error while parsing the relay config file. No relays setup in relayconfig.xml."
};

// need to maintain numbers to update kNumMessages in QTSSMessages.h.

void QTSSMessages::Initialize()
{
	QTSSDictionaryMap* theMap = QTSSDictionaryMap::GetMap(QTSSDictionaryMap::kTextMessagesDictIndex);
	Assert(theMap != NULL);
    
	for (UInt32 x = 0; x < qtssMsgNumParams; x++)
		theMap->SetAttribute(x, sMessagesKeyStrings[x], NULL, qtssAttrDataTypeCharArray,
				     qtssAttrModeRead | qtssAttrModePreempSafe);
}

QTSSMessages::QTSSMessages(PrefsSource* inMessages)
: QTSSDictionary(QTSSDictionaryMap::GetMap(QTSSDictionaryMap::kTextMessagesDictIndex))
{
	static const UInt32 kMaxMessageSize = 2048;
	char theMessage[kMaxMessageSize];
    
	// Use the names of the attributes in the attribute map as the key values for
	// finding preferences in the config file.
    
	for (UInt32 x = 0; x < this->GetDictionaryMap()->GetNumAttrs(); x++) {
		theMessage[0] = '\0';
		(void)inMessages->GetValue(this->GetDictionaryMap()->GetAttrName(x), &theMessage[0]);
        
		if (theMessage[0] == '\0') {
			// If a message doesn't exist in the file, check to see if this attribute
			// name matches one of the compiled-in strings. If so, use that instead
			for (UInt32 y = 0; y < kNumMessages; y++) {
				if (::strcmp(this->GetDictionaryMap()->GetAttrName(x), sMessagesKeyStrings[y]) == 0) {
					::strcpy(theMessage, sMessages[y]);
					break;
				}
			}
			// If we didn't find a match, just copy in this last-resort message
			if (theMessage[0] == '\0')
				::strcpy(theMessage, "No Message");
		}
        
		// Add this preference into the dictionary.
        
		// If there is a message, allocate some new memory for 
		// the new attribute, and copy the data into the newly allocated buffer
		if (theMessage[0] != '\0') {
			char* attrBuffer = NEW char[::strlen(theMessage) + 2];
			::strcpy(attrBuffer, theMessage);
			this->SetVal(this->GetDictionaryMap()->GetAttrID(x), attrBuffer, ::strlen(attrBuffer));
		}
	}
}

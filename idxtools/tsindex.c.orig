
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "version.h"
#include "es_fns.h"
#include "pes_fns.h"
#include "ts_fns.h"
#include "h262_fns.h"
#include "misc_fns.h"
#include "index_fns.h"

#define TS_INDEX_ID "tsidx"
#define TS_INDEX_ID_LEN 5
#define TS_INDEX_VER_MAIN 1
#define TS_INDEX_VER_MIN 0
#define TS_INDEX_VER_REVISION 0

static void index_init (struct tsindex *idx)
{
    memset (idx, 0, sizeof(struct tsindex));
    memcpy (idx->id, TS_INDEX_ID, 5);
    idx->ver[0] = TS_INDEX_VER_MAIN;
    idx->ver[1] = TS_INDEX_VER_MIN;
    idx->ver[2] = TS_INDEX_VER_REVISION;
}


/*
 * Analyze on the content of an MPEG2 file
 *
 * - `es` is the input elementary stream
 */
static int analyze_h262_by_frame (ES_p es, struct tsindex *idx)
{
    int  err;
    int  count = 0;
    int  num_frames = 0;
    int  num_sequence_headers = 0;
    int  num_sequence_ends = 0;
    int  num_x_frames[4] = {0,0,0,0};

    h262_context_p  h262;
    h262_picture_p  picture;

    err = build_h262_context(es,&h262);
    if (err)
    {
        print_err("### Error trying to build H.262 reader from ES reader\n");
        return -1;
    }

    for (;;)
    {
        err = get_next_h262_frame (h262, FALSE, FALSE, &picture);
        if (err == EOF)
            break;
        else if (err)
        {
            print_err("### Error getting next H.262 picture\n");
            break;
        }
        count++;

        if (picture->is_picture)
        {
            num_frames ++;
            if (picture->picture_coding_type < 5 &&
                picture->picture_coding_type > 0)  // paranoia - check for array bounds
                num_x_frames[picture->picture_coding_type - 1] ++;
            if (picture->picture_coding_type == 1)
            { // I frame founded
                idx->indexr[num_x_frames[0]-1].pos = es->last_packet_posn;
                idx->indexr[num_x_frames[0]-1].ifi = num_frames;
            }
        }
        else if (picture->is_sequence_header)
        {
            
            if (picture->frame_rate_info == 0) {
                print_err ("### Error frame rate info in ts file\n");
                return -1;
            }
            else if (idx->framerate == 0)
            {
                idx->framerate = picture->frame_rate_info;
            }
            else if (picture->frame_rate_info != idx->framerate)
            {// should be fixed frame rate
                print_err ("### Not a fixed frame rate ts\n");
                return -1;
            }
            num_sequence_headers ++;
        }
        else
            num_sequence_ends ++;

        free_h262_picture(&picture);
    
    }

    free_h262_context(&h262);

    // ts index information
    idx->ifn = num_x_frames[0];
    idx->fn = num_frames;

    //calculate movie duration
    if (idx->framerate == 1)
    {
        idx->duration = (uint64_t)(((double)idx->fn / (24000./1001)) * 1000000);
    }
    else if (idx->framerate == 2)
    {
        idx->duration = (uint64_t)(((double)idx->fn / 24) * 1000000);
    }
    else if (idx->framerate == 3)
    {
        idx->duration = (uint64_t)(((double)idx->fn / 25) * 1000000);
    }
    else if (idx->framerate == 4)
    {
        idx->duration = (uint64_t)(((double)idx->fn / (30000./1001)) * 1000000);
    }
    else if (idx->framerate == 5)
    {
        idx->duration = (uint64_t)(((double)idx->fn / 30) * 1000000);
    }    

    // calculate GOP duration.
    idx->gt = idx->duration / idx->ifn;

    return 0;
}

static void print_usage ()
{
    print_msg(
        "Usage: tsindex [<infile>] [<outfile>]\n"
        "\n"
        );
    REPORT_VERSION("tsindex");
    print_msg(
        "\n"
        "  -stdin            Take input from <stdin>, instead of a named file\n"
        "  -stdout           Write output to <stdout>, instead of a named file\n"
        "                    Forces -quiet and -err stderr.\n"
        "\n"
        );
}

int main(int argc, char** argv)
{
    int had_input_name = FALSE;
    int had_output_name = FALSE;
    char *input_name = NULL;
    char *output_name = NULL;
    int use_stdin = FALSE;
    int use_stdout = FALSE;
    int err = 0;
    int is_data;
    ES_p es = NULL;

    int ii = 1;

    struct tsindex tsidx;

    index_init (&tsidx);

    if (argc < 2)
    {
        print_usage ();
        return 0;
    }

    while (ii < argc)
    {
        if (argv[ii][0] == '-')
        {
            if (!strcmp ("--help", argv[ii]) || !strcmp ("-help", argv[ii]) || !strcmp ("-h", argv[ii]))
            {
                print_usage ();
                return 0;
            }
            else if (!strcmp("-stdin",argv[ii]))
            {
                had_input_name = TRUE; // more or less
                use_stdin = TRUE;
            }
            else if (!strcmp("-stdout",argv[ii]))
            {
                had_output_name = TRUE; // more or less
                use_stdout = TRUE;
            }
        }
        else
        {
            if (had_input_name && had_output_name)
            {
                fprint_err("### esfilter: Unexpected '%s'\n",argv[ii]);
                return 1;
            }
            else if (had_input_name)
            {
                output_name = argv[ii];
                had_output_name = TRUE;
            }
            else
            {
                input_name = argv[ii];
                had_input_name = TRUE;
            }
        }
        ii++;
    }

    if (!had_input_name)
    {
        print_err("### esfilter: No input file specified\n");
        return 1;
    }

    if (!had_output_name)
    {
        print_err("### esfilter: No output file specified\n");
        return 1;
    }

    err = open_input_as_ES((use_stdin?NULL:input_name),
                           TRUE, // use_pes
                           TRUE, // quiet
                           FALSE, //force_stream_type,
                           VIDEO_H262, //want_data,
                           &is_data,
                           &es);
    if (err)
    {
        print_err("### esfilter: Error opening input file\n");
        return 1;
    }

    if (is_data == VIDEO_H262)
    {
        print_msg ("Movie type: MPEG2\n");

        tsidx.vfmt = MPEG2_VIDEO_STREAM_TYPE;
        if (0 == analyze_h262_by_frame (es, &tsidx))
        {
            fprint_msg ("total frame sum: %d\n", tsidx.fn);
            fprint_msg ("I frame sum: %d\n", tsidx.ifn);
            fprint_msg ("Frame rate: %d\n", tsidx.framerate);
            fprint_msg ("GOP time: %d\n", tsidx.gt);
            fprint_msg ("Movie duration: %lldus\n", tsidx.duration);
            fprint_msg ("Generate index file ver: %d.%d.%d\n", tsidx.ver[0], tsidx.ver[1], tsidx.ver[2]);

            if (!use_stdout)
            {
                if (0 != write_tsindex_file (output_name, &tsidx))
                {
                    fprint_err ("### Create index file failure\n");

                    return -1;
                }
            }

            for (ii = 0; ii < tsidx.ifn; ii++)
                fprint_msg ("I frame, position: %lld; idx: %d\n", tsidx.indexr[ii].pos, tsidx.indexr[ii].ifi);
        }
        else
        {
            print_err("### encounter error, stop process\n");
        }
    }
    else if (is_data == VIDEO_H264)
    {
        print_msg ("H.264 file founded\n");
    }

    err = close_input_as_ES(input_name,&es);
    if (err)
    {
        print_err("### esreport: Error closing input file\n");
        return 1;
    }

    return 0;
}

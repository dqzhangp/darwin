
#include <stdio.h>
#include <stdlib.h>

#include "index_fns.h"

int write_tsindex_file (char* file, struct tsindex *idx)
{
    FILE *f;
    uint64_t data_len;
    uint64_t len;
    
    f = fopen (file, "wb");

    if (f == NULL)
    {
        fprint_err ("### Unable to create index file %s\n", file);
        return -1;
    }

    data_len = sizeof(struct tsindex) - (MAX_RECORD_NUM - idx->ifn) * sizeof (struct idxrecord);

    len = fwrite(idx, 1, data_len, f);

    if (len != data_len)
    {
        fprint_err ("### Write index content error\n");
        fclose (f);
        return -1;
    }

    fclose (f);

    return 0;
}

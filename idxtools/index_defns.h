
#ifndef _index_defns
#define _index_defns

#include "compat.h"

#define MAX_RECORD_NUM 64*1024

struct idxrecord {
    uint64_t pos;
    uint32_t num; // the number of ts packets this I frame contain 
    uint32_t ifi; // I frame index number
};

struct tsindex {
    char id[5];  // ts index file mark, should be tsidx
    char ver[3]; // three byte: x.y.z
    char reserve[4]; // lp64
    uint8_t vbr; // 1 is vbr, otherwise is cbr
    uint8_t framerate; // framerate
    uint8_t vfmt; // video format
    uint8_t afmt; // audio format
    uint16_t vpid; // video pid
    uint16_t apid; // audio pid
    uint32_t tps; // TS packet size, normally 188
    uint32_t fn; // frame number
    uint32_t ifn; // I frame number or idr number for h264
    uint64_t length; // file length in bytes
    uint64_t duration; // movie duration, us
    uint64_t gt; // gop time, us;
    uint64_t avrgbitrat; // bitrate
    char pat[188]; // pat
    char pmt[188]; // pmt
    struct idxrecord indexr[MAX_RECORD_NUM];
};

#endif // _index_defns

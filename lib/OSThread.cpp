/*
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * Copyright (c) 1999-2008 Apple Inc.  All Rights Reserved.
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 *
 */
/*
    File:       OSThread.cpp

    Contains:   Thread abstraction implementation

    
    
*/

#include "QTSS.h"
#include <stdio.h>
#include <stdlib.h>
#include "SafeStdLib.h"
#include <string.h>
#include <errno.h>

#ifdef __MacOSX__
#include <mach/mach_types.h>
#include <mach/mach_time.h>
#endif

    #if HAVE_PTHREAD
        #include <pthread.h>
    #else
        #include <mach/mach.h>
        #include <mach/cthreads.h>
    #endif
    #include <unistd.h>
    #include <grp.h>
    #include <pwd.h>

#include "OSThread.h"
#include "MyAssert.h"

#ifdef __sgi__ 
#include <time.h>
#endif

//
// OSThread.cp
//
void*   OSThread::sMainThreadData = NULL;

pthread_key_t OSThread::gMainKey = 0;
#ifdef _POSIX_THREAD_PRIORITY_SCHEDULING
pthread_attr_t OSThread::sThreadAttr;
#endif

char  OSThread::sUser[128]= "";
char  OSThread::sGroup[128]= "";

#if __linux__ ||  __MacOSX__
Bool16  OSThread::sWrapSleep = true;
#endif

void OSThread::Initialize()
{
    pthread_key_create(&OSThread::gMainKey, NULL);
#ifdef _POSIX_THREAD_PRIORITY_SCHEDULING

    //
    // Added for Solaris...
    
    pthread_attr_init(&sThreadAttr);
    /* Indicate we want system scheduling contention scope. This
       thread is permanently "bound" to an LWP */
    pthread_attr_setscope(&sThreadAttr, PTHREAD_SCOPE_SYSTEM);
#endif
}

OSThread::OSThread()
    : fStopRequested(false),
      fJoined(false),
      fThreadData(NULL)
{
}

OSThread::~OSThread()
{
    this->StopAndWaitForThread();
}

void OSThread::Start()
{
    pthread_attr_t* theAttrP;
#ifdef _POSIX_THREAD_PRIORITY_SCHEDULING
    //theAttrP = &sThreadAttr;
    theAttrP = 0;
#else
    theAttrP = NULL;
#endif
    int err = pthread_create((pthread_t*)&fThreadID, theAttrP, _Entry, (void*)this);
    Assert(err == 0);
}

void OSThread::StopAndWaitForThread()
{
    fStopRequested = true;
    if (!fJoined)
        Join();
}

void OSThread::Join()
{
    // What we're trying to do is allow the thread we want to delete to complete
    // running. So we wait for it to stop.
    Assert(!fJoined);
    fJoined = true;
    void *retVal;
    pthread_join((pthread_t)fThreadID, &retVal);
}

void OSThread::ThreadYield()
{
    // on platforms who's threading is not pre-emptive yield 
    // to another thread
#if THREADING_IS_COOPERATIVE
    #if HAVE_PTHREAD
            sched_yield();
    #endif
#endif
}

#include "OS.h"
void OSThread::Sleep(UInt32 inMsec)
{
    if (inMsec == 0)
        return;

    SInt64 startTime = OS::Milliseconds();
    SInt64 timeLeft = inMsec;
    SInt64 timeSlept = 0;
    UInt64 utimeLeft = 0;

    do { // loop in case we leave the sleep early
        //qtss_printf("OSThread::Sleep time slept= %qd request sleep=%qd\n",timeSlept, timeLeft);
        timeLeft = inMsec - timeSlept;
        if (timeLeft < 1)
            break;

        utimeLeft = timeLeft * 1000;    
        //qtss_printf("OSThread::Sleep usleep=%qd\n", utimeLeft);
        ::usleep(utimeLeft);

        timeSlept = (OS::Milliseconds() - startTime);
        if (timeSlept < 0) // system time set backwards
            break;
    } while (timeSlept < inMsec);

    //qtss_printf("total sleep = %qd request sleep=%"_U32BITARG_"\n", timeSlept,inMsec);
}

void* OSThread::_Entry(void *inThread)  //static
{
    OSThread* theThread = (OSThread*)inThread;
    theThread->fThreadID = (pthread_t)pthread_self();
    pthread_setspecific(OSThread::gMainKey, theThread);
    theThread->SwitchPersonality();
    //
    // Run the thread
    theThread->Entry();
    return NULL;
}

Bool16  OSThread::SwitchPersonality()
{
   if (::strlen(sGroup) > 0) {
        struct group* gr = ::getgrnam(sGroup);
        if (gr == NULL || ::setgid(gr->gr_gid) == -1) {
            //qtss_printf("thread %"_U32BITARG_" setgid  to group=%s FAILED \n", (UInt32) this, sGroup);
            return false;
        }
        
        //qtss_printf("thread %"_U32BITARG_" setgid  to group=%s \n", (UInt32) this, sGroup);
    }

    if (::strlen(sUser) > 0) {
        struct passwd* pw = ::getpwnam(sUser);
        if (pw == NULL || ::setuid(pw->pw_uid) == -1) {
            //qtss_printf("thread %"_U32BITARG_" setuid  to user=%s FAILED \n", (UInt32) this, sUser);
            return false;
        }

        //qtss_printf("thread %"_U32BITARG_" setuid  to user=%s \n", (UInt32) this, sUser);
   }

   return true;
}

OSThread* OSThread::GetCurrent()
{
    return (OSThread *)pthread_getspecific(OSThread::gMainKey);
}


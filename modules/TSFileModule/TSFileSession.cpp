
#include "OSMemory.h"
#include "TSFileSession.h"
#include "QTSSMemoryDeleter.h"

TSFileSession::TSFileSession (char *path)
    : fFilePath (path),
      fFile (NULL),
      fNextPacketLen(0),
      fLastQualityCheck(0),
      fAllowNegativeTTs(false),
      fSpeed(1),
      fCurrentPlayTime(0),
      fStopTrackID(0),
      fStopPN(0),
      fLastRTPTime(0),
      fLastPauseTime(0),
      fTotalPauseTime(0),
      fPaused(true),
      fAdjustPauseTime(true),
      fEOS(false),
      fLastPlayStartTime(0),
      fLastPlayStopTime(-1)
{ 
    fPacket.packetData = NULL;
    fPacket.packetTransmitTime = -1; 
    fPacket.suggestedWakeupTime = -1;
}

TSFileSession::~TSFileSession ()
{
    if (fFile != NULL) {
        QTSS_CloseFileObject (fFile);
        fFile = NULL;
    }
}

QTSS_Error TSFileSession::Initialize()
{
    static UInt16 BufSize = 200;
    QTSSCharArrayDeleter tempBuf (NEW char[BufSize]);
    QTSS_Error theReturn;

    // The Index file path
    qtss_sprintf (tempBuf, "%s", fFilePath);
    qtss_sprintf (&tempBuf[strlen (fFilePath) - 2], "idx");
    // tempBuf[requestPath.Len + 2] = '\0';
    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: the index file: %s", (char *)tempBuf);

    theReturn = QTSS_OpenFileObject (fFilePath, qtssOpenFileReadAhead, &fFile);
    if (theReturn != QTSS_NoErr) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: open movie file %s failure", fFilePath);
        return QTSS_FileNotFound;
    }

    theReturn = fIndex.InitializeIndex (tempBuf);
    if (theReturn != QTSS_NoErr) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: open index of movie file %s failure", fFilePath);
        return QTSS_BadIndex;
    }

    fDuration = fIndex.GetDuration () / 1000; // ms
    MoveToBOS ();

/*    // calculate FrameRate 
    if (fIndex.idx.FrameRate == 1)
    {
        FrameRate = 24000./1001;
    }
    else if (fIndex.idx.FrameRate == 2)
    {
        FrameRate = 24;
    }
    else if (fIndex.idx.FrameRate == 3)
    {
        FrameRate = 25;
    }
    else if (fIndex.idx.FrameRate == 4)
    {
        FrameRate = 30000./1001;
    }
    else if (fIndex.idx.FrameRate == 5)
    {
        FrameRate = 30;
    }
*/
    if (NextIFrameNum <= CurrentIFrameNum) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: bad index file");
        return QTSS_BadIndex;
    }
/*
    QTSS_Log (qtssWarningVerbosity, "[TSFileModule]:\n"
              "CurrentIFrameTime: %lld\n"
              "CurrentIFramePos:%lld\n"
              "NextIFramePos:%lld\n"
              "CurrentIFrameNum:%d\n"
              "NextIFrameNum:%d\n"
              "CurrentByteRate:%d",
              CurrentIFrameTime,
              CurrentIFramePos,
              NextIFramePos,
              CurrentIFrameNum,
              NextIFrameNum,
              CurrentByteRate);
*/
    return QTSS_NoErr;
}

void TSFileSession::MoveToBOS ()
{
    CurrentIFrameTime = 0;
    CurrentIFramePos = 0;
    NextIFramePos = fIndex.idx.Indexr[1].Pos;
    CurrentIFrameNum = 0;
    NextIFrameNum = fIndex.idx.Indexr[1].Ifi;
    CurrentTime = 0;
    CurrentPos = 0;
    CurrentRecordNum = 0;
    FrameRate = fIndex.GetFrameRate ();
    CurrentByteRate = ((NextIFramePos - CurrentIFramePos) * FrameRate) / (NextIFrameNum - CurrentIFrameNum);

    QTSS_Seek(fFile, 0);
}

void TSFileSession::MoveToNextGOP ()
{
/*
  record number: n
  the last record number: n-1
  if the current record number is i, then the next record number is i+1, and i+1<=n-1: i <= n-2
 */
    if (CurrentRecordNum < (fIndex.idx.Ifn - 2)) {
        CurrentRecordNum++; // move to Next record
        CurrentIFrameNum = NextIFrameNum;
        NextIFrameNum = fIndex.idx.Indexr[CurrentRecordNum+1].Ifi;
        CurrentIFramePos = NextIFramePos;
        NextIFramePos = fIndex.idx.Indexr[CurrentRecordNum+1].Pos;
        CurrentByteRate = ((NextIFramePos - CurrentIFramePos) * FrameRate) / (NextIFrameNum - CurrentIFrameNum);
        CurrentIFrameTime = CurrentIFrameNum * 1000 / FrameRate;
    }
}

void TSFileSession::MoveToGOP (UInt32 theRecordNum)
{
    CurrentRecordNum = theRecordNum; // move to the record
    CurrentIFrameNum = fIndex.idx.Indexr[CurrentRecordNum].Ifi;
    NextIFrameNum = fIndex.idx.Indexr[CurrentRecordNum+1].Ifi;
    CurrentIFramePos = fIndex.idx.Indexr[CurrentRecordNum].Pos;
    NextIFramePos = fIndex.idx.Indexr[CurrentRecordNum+1].Pos;
    CurrentByteRate = ((NextIFramePos - CurrentIFramePos) * FrameRate) / (NextIFrameNum - CurrentIFrameNum);
    CurrentIFrameTime = CurrentIFrameNum * 1000 / FrameRate;
    CurrentPos = CurrentIFramePos;

    QTSS_Seek(fFile, CurrentPos);
}

SInt64 TSFileSession::GetNextPacket ()
{
    QTSS_Error theReturn;
/*
    QTSS_Log (qtssMessageVerbosity, "[TSFileModule]:\n"
              "CurrentIFrameTime: %lld\n"
              "CurrentIFramePos:%lld\n"
              "NextIFramePos:%lld\n"
              "CurrentIFrameNum:%d\n"
              "NextIFrameNum:%d\n"
              "CurrentByteRate:%d\n"
              "CurrentPos:%lld",
              CurrentIFrameTime,
              CurrentIFramePos,
              NextIFramePos,
              CurrentIFrameNum,
              NextIFrameNum,
              CurrentByteRate,
              CurrentPos);
*/
    if (CurrentPos >= NextIFramePos) {
        MoveToNextGOP ();
    }

    UInt32 gotLength;
    // fReadBuffer[12], 12 bytes for RTP header
    gotLength = 0;
    theReturn = QTSS_Read (fFile, &fReadBuffer[12], 1316, &gotLength);

    fNextPacketLen = gotLength;
    if ((gotLength == 0) || (theReturn == QTSS_NoMoreData)) { // No more data, return
        fEOS = true;
        return -1;
    }

    fPacket.packetData = fSendPacketPtr;
    fCurrentPlayTime = (SInt64)(CurrentIFrameTime + ((CurrentPos - CurrentIFramePos) * 1000)/CurrentByteRate);
    CurrentPos += UDP_PACKET_SIZE;

    return fCurrentPlayTime;
}

/*
  seek to the nearest I frame before the seek time
  # I frame
  * other frame
  $ seek time
  
  case 1:
  ......#---*---*---*---*--$*---#......
       /|\
        |
   seek to here
 
  case 2:
  ......#---*$--*---*---*---*---#......
       /|\
        |
    seek to here

  case 3:
        $
  ......#---*---*---*---*---*---#......
       /|\
        |
    seek to here
*/
SInt64 TSFileSession::Seek (UInt64 theSeekTime /*ms*/)
{
    UInt32 theCorrespondIFrameNum;
    UInt32 theCorrespondFrameNum;

    theCorrespondFrameNum = (theSeekTime / 1000) * FrameRate;
    // Gt means average time(us) of a GOP
    theCorrespondIFrameNum = (theSeekTime * 1000) / fIndex.GetGOPTime();

    if (theCorrespondIFrameNum == 0) {
        MoveToBOS ();
        return QTSS_NoErr;
    }

    if (theCorrespondIFrameNum >= fIndex.GetIFrameNum () - 1) {
        fEOS = true;
        return fIndex.GetDuration () / 1000; // seconds
    }

    if (fIndex.idx.Indexr[theCorrespondIFrameNum].Ifi > theCorrespondFrameNum) {
        while (fIndex.idx.Indexr[theCorrespondIFrameNum].Ifi > theCorrespondFrameNum) {
            theCorrespondIFrameNum -= 1;
        }
    } else if (fIndex.idx.Indexr[theCorrespondIFrameNum].Ifi < theCorrespondFrameNum) {
        while (fIndex.idx.Indexr[theCorrespondIFrameNum].Ifi < theCorrespondFrameNum) {
            theCorrespondIFrameNum += 1;
        }
    }

    MoveToGOP (theCorrespondIFrameNum);
    fCurrentPlayTime = (SInt64)(CurrentIFrameTime + ((CurrentPos - CurrentIFramePos) * 1000)/CurrentByteRate);

    return fCurrentPlayTime;
}

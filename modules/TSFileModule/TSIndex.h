
#ifndef TSIndex_H
#define TSIndex_H

#include "OSHeaders.h"
#include "QTSS.h"

#define MAX_RECORD_NUM 64*1024

class TSIndex {

public:

    // the same as in index_defns.h of idxtools
    struct IndexRecord {
        UInt64 Pos;
        UInt32 Num;
        UInt32 Ifi;
    };

    // the same as in index_defns.h of idxtools
    struct Index {
        char Id[5];
        char Ver[3];
        char reserve[4];
        UInt8 Vbr;
        UInt8 FrameRate;
        UInt8 Vfmt;
        UInt8 Afmt;
        UInt16 Vpid;
        UInt16 Apid;
        UInt32 Tps;
        UInt32 Fn;
        UInt32 Ifn;
        UInt64 Length;
        UInt64 Duration;
        UInt64 Gt;
        UInt64 Avrgbitrat;
        char pat[188];
        char pmt[188];
        struct IndexRecord Indexr[MAX_RECORD_NUM];
    };

    TSIndex ();
    virtual ~TSIndex (void);

    // Check Index File validation, by Index file id: tsidx
    Bool16 isValidIndexFile ();

    QTSS_Error InitializeIndex (char *fTSIndexPath);

    // Movie start time : 0 obviously
    UInt64 GetStartTime ();
    UInt64 GetDuration ();
    // GOP time
    UInt64 GetGOPTime ();

    //Get video type
    UInt8 GetVideoType ();

    //Get audio type
    UInt8 GetAudioType ();

    // Get video frame rate
    Float64 GetFrameRate ();

    // Get I Frame Number
    UInt32 GetIFrameNum ();

    // Index File Version
    char* GetVersion ();

    struct Index idx;

protected:

    char Version[12];
};

#endif //TSIndex_H


#ifndef TSFILESESSION_H
#define TSFILESESSION_H

#include "OSHeaders.h"
#include "QTSS.h"
#include "TSIndex.h"
#include "SDPSourceInfo.h"

#define UDP_PACKET_SIZE 1316 // the size read from file once

enum { // TransportType enum
    RTPAVPTransport = 1,
    RAWUDPTransport = 2,
    IPQAMTransport = 3
};

class TSFileSession
{
public:

    TSFileSession (char *thePath);
        
    ~TSFileSession (); 

    QTSS_Error Initialize ();
    SInt64 GetNextPacket ();
    // the return value is the real position afte seek(seek 1, the result maybe 0.9, because random access point)
    SInt64 Seek (UInt64 theSeekTime /*ms*/);

    Float64 GetLastPlayStartTime ()
    {
        return fLastPlayStartTime;
    }

    void SetLastPlayStartTime (Float64 theTime)
    {
        fLastPlayStartTime = theTime;
    }

    Float64 GetLastPlayStopTime ()
    {
        return fLastPlayStopTime;
    }

    void SetLastPlayStopTime (Float64 theTime)
    {
        fLastPlayStopTime = theTime;
    }

    SInt64 GetCurrentPlayTime ()
    {
        return fCurrentPlayTime;
    }

    Float64 GetDuration ()
    {
        return fDuration;
    }

    char* fFilePath;
    class TSIndex fIndex;
    QTSS_Object fFile;
    char fReadBuffer[1500];
    char* fSendPacketPtr;
    QTSS_PacketStruct fPacket;
    int fNextPacketLen;
    SInt64 fLastQualityCheck;
    SDPSourceInfo fSDPSource;
    Bool16 fAllowNegativeTTs;
    UInt32 fTransportType;
    UInt16 fTransportPayloadSize;
    Float32 fSpeed;
    SInt64 fCurrentPlayTime;
    UInt16 fFirstSeqNum;
    UInt32 fFirstTimestamp;
    UInt32 fStopTrackID; // no use
    UInt64 fStopPN;
    UInt32 fLastRTPTime;
    UInt64 fLastPauseTime;
    SInt64 fTotalPauseTime;
    Bool16 fPaused;
    Bool16 fAdjustPauseTime;
    Bool16 fEOS;

private:
    // refresh CurrentIFramePos, NextIFramePos, CurrentIFrameNum, NextFrameNum, CurrentByteRate
    void MoveToNextGOP ();
    void MoveToGOP (UInt32 theIframeNum);
    void MoveToBOS ();

    // CurrentIFrameTime = CurrentIFrameNum / FrameRate
    UInt64 CurrentIFrameTime;
    // CurrentByteRate = (NextIFramePos - CurrentIFramePos)/((NextIFrameNum - CurrentIFrameNum) / FrameRate)
    UInt64 CurrentIFramePos;
    UInt64 NextIFramePos;
    UInt32 CurrentIFrameNum;
    UInt32 NextIFrameNum;
    UInt32 CurrentByteRate;
    // CurrentTime = CurrentIFrameTime + (CurrentPos - CurrentIFramePos)/CurrentByteRate
    UInt64 CurrentTime;
    UInt64 CurrentPos;
    // Current record number means I frame number in I frames only
    UInt32 CurrentRecordNum;
    Float64 FrameRate;

    Float64 fDuration;
    Float64 fLastPlayStartTime;
    Float64 fLastPlayStopTime;
};

#endif //TSFILESESSION_H

#ifndef _TSFILEMODULE_H_
#define _TSFILEMODULE_H_

#include "QTSS.h"

extern "C"
{
    QTSS_Error TSFileModule_Main (void* inPrivateArgs);
}

#endif //_TSFILEMODULE_H_

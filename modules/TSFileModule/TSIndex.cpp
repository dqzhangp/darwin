
#include "TSIndex.h"
#include "SafeStdLib.h"
#include "string.h"

TSIndex::TSIndex ()
{

}

TSIndex::~TSIndex (void)
{
}

Bool16 TSIndex::isValidIndexFile ()
{
    // Header tsidx
    if ((idx.Id[0] != 't') || (idx.Id[1] != 's') || (idx.Id[2] != 'i') || (idx.Id[3] != 'd') || (idx.Id[4] != 'x')) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: invalid index header");
        return false;
    }

    // Version, now is 1.0.0
    if ((idx.Ver[0] != 1) || (idx.Ver[1] != 0) || (idx.Ver[2] != 0)) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: invalid version of index file");
        return false;
    }

    // duration, should great than 0
    if (idx.Duration == 0) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: index file: duration is 0!");
        return false;
    }

    if ((idx.FrameRate < 1) || (idx.FrameRate > 8)) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: Invalid frame rate");
        return false;
    }
    return true;
}

QTSS_Error TSIndex::InitializeIndex (char *fTSIndexPath)
{
    QTSS_Object fTSIndexFD;
    QTSS_Error theReturn;
    UInt32 fReadLen;

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: open index file %s", fTSIndexPath);

    theReturn = QTSS_OpenFileObject (fTSIndexPath, qtssOpenFileReadAhead, &fTSIndexFD);
    if (theReturn != QTSS_NoErr) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: open index file %s failure", fTSIndexPath);
        return QTSS_RequestFailed;
    }

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: read index file %s", fTSIndexPath);
    theReturn = QTSS_Read (fTSIndexFD, &idx, sizeof(idx), &fReadLen);
    if (fReadLen <= 0) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: read index file %s failure", fTSIndexPath);
        return QTSS_RequestFailed;
    }

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: close index file %s", fTSIndexPath);
    (void)QTSS_CloseFileObject (fTSIndexFD);

    if (true == isValidIndexFile()) {
        ::memset (&Version[0], 0, 12);
        qtss_sprintf ((char *)&Version[0], "%d.%d.%d", idx.Ver[0], idx.Ver[1], idx.Ver[2]);
        return QTSS_NoErr;
    } else
        return QTSS_BadIndex;
}

UInt64 TSIndex::GetStartTime ()
{
    return 0;
}

UInt64 TSIndex::GetDuration ()
{
    return idx.Duration;
}

UInt8 TSIndex::GetVideoType ()
{
    return idx.Vfmt;
}

UInt8 TSIndex::GetAudioType ()
{
    return idx.Afmt;
}

Float64 TSIndex::GetFrameRate ()
{
    return ((idx.FrameRate)==1? 24000./1001:
            (idx.FrameRate)==2? 24:
            (idx.FrameRate)==3? 25:
            (idx.FrameRate)==4? 30000./1001:
            (idx.FrameRate)==5? 30:
            (idx.FrameRate)==6? 50:
            (idx.FrameRate)==7? 60000./1001:
            (idx.FrameRate)==8? 60:0);
}

UInt64 TSIndex::GetGOPTime ()
{
    return idx.Gt;
}

UInt32 TSIndex::GetIFrameNum ()
{
    return idx.Ifn;
}

char* TSIndex::GetVersion ()
{
    return (char *)&Version[0];
}

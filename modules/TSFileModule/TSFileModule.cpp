
#include "ResizeableStringFormatter.h"
#include "OSArrayObjectDeleter.h"
#include "QTSSModuleUtils.h"
#include "QTSSMemoryDeleter.h"
#include "TSFileModule.h"
#include "TSFileSession.h"
#include "TSIndex.h"
#include "OSMemory.h"
#include "QTSS.h"

static StrPtrLen sEOL ((char *)"\r\n");
static StrPtrLen sTSSuffix ((char *)".ts");
static StrPtrLen sSDPVersionHeader ((char *)"v=0");
static StrPtrLen sSDPSessionNameHeader ((char *)"s=");
static StrPtrLen sPermanentTimeHeader((char *)"t=0 0");
static StrPtrLen sConnectionHeader((char *)"c=IN IP4 0.0.0.0");
static StrPtrLen sSDPAttrRangeHeader ((char *)"a=range:npt=");
static StrPtrLen sSDPMediaNameHeader ((char *)"m=video 0 RTP/AVP 33"); // take it as a preference?
static StrPtrLen sSDPRTPMAPAttrib ((char *)"a=rtpmap:33 MP2T/90000");
static StrPtrLen sSDPControlHeader ((char *)"a=control:trackID=1");
static StrPtrLen sSDPAttrFormateParaHeader ((char *)"a=fmtp:33");

// ATTRIBUTES IDs
static QTSS_AttributeID sTSFileSessionAttr = qtssIllegalAttrID;
static QTSS_AttributeID sNoIndexFileFoundErr = qtssIllegalAttrID;

static UInt32 sFlowControlProbeInterval = 10; //TODO ??

// FUNCTIONS
static QTSS_Error TSFileModuleDispatch (QTSS_Role inRole, QTSS_RoleParamPtr inParamBlock);
static QTSS_Error Register (QTSS_Register_Params* inParams);
static QTSS_Error Initialize (QTSS_Initialize_Params* inParamBlock);
static QTSS_Error ProcessRTSPRequest (QTSS_StandardRTSP_Params* inParamBlock);
static QTSS_Error DoDescribe (QTSS_StandardRTSP_Params* inParamBlock);
static QTSS_Error DoSetup(QTSS_StandardRTSP_Params* inParamBlock);
static QTSS_Error DoPlay(QTSS_StandardRTSP_Params* inParamBlock);
static QTSS_Error DoPause(QTSS_StandardRTSP_Params* inParamBlock);
static QTSS_Error DoGetParameterMethod(QTSS_StandardRTSP_Params* inParamBlock);
static QTSS_Error SendPackets(QTSS_RTPSendPackets_Params* inParams);
static QTSS_Error DestroySession (QTSS_ClientSessionClosing_Params* inParams);

QTSS_Error TSFileModule_Main (void* inPrivateArgs)
{
    return _stublibrary_main (inPrivateArgs, TSFileModuleDispatch);
}

QTSS_Error TSFileModuleDispatch (QTSS_Role inRole, QTSS_RoleParamPtr inParamBlock)
{
    switch (inRole) {
    case QTSS_Register_Role:
        return Register (&inParamBlock->regParams);
    case QTSS_Initialize_Role:
        return Initialize (&inParamBlock->initParams);
    case QTSS_RTSPPreProcessor_Role:
        return ProcessRTSPRequest (&inParamBlock->rtspRequestParams);
    case QTSS_RTPSendPackets_Role:
        return SendPackets(&inParamBlock->rtpSendPacketsParams);
    case QTSS_ClientSessionClosing_Role:
        return DestroySession (&inParamBlock->clientSessionClosingParams);
    }
    return QTSS_NoErr;
}

QTSS_Error Register (QTSS_Register_Params* inParams)
{
    static char* sNoIndexFileFoundName = (char *)"TSFileModuleNoIndexFileFound";

    // Register for roles
    (void)QTSS_AddRole(QTSS_Initialize_Role);
    (void)QTSS_AddRole(QTSS_RTSPPreProcessor_Role);
    (void)QTSS_AddRole(QTSS_ClientSessionClosing_Role);

    ::strcpy (inParams->outModuleName, (char *)"TSFileModule");

    (void)QTSS_AddStaticAttribute(qtssTextMessagesObjectType, sNoIndexFileFoundName, NULL, qtssAttrDataTypeCharArray);
    (void)QTSS_IDForAttr(qtssTextMessagesObjectType, sNoIndexFileFoundName, &sNoIndexFileFoundErr);

    // Add an Client session attribute for tracking FileSession objects
    static char* sTSFileSessionName = (char *)"TSFileModuleSession";
    (void)QTSS_AddStaticAttribute(qtssClientSessionObjectType, sTSFileSessionName, NULL, qtssAttrDataTypeVoidPointer);
    (void)QTSS_IDForAttr(qtssClientSessionObjectType, sTSFileSessionName, &sTSFileSessionAttr);

    return QTSS_NoErr;
}

QTSS_Error Initialize(QTSS_Initialize_Params* inParams)
{
    QTSSModuleUtils::Initialize(inParams->inMessages, inParams->inServer, inParams->inErrorLogStream);

    // Report to the server that this module handles DESCRIBE, SETUP, PLAY, PAUSE, and TEARDOWN
    static QTSS_RTSPMethod sSupportedMethods[] = { 
        qtssDescribeMethod, 
        qtssSetupMethod, 
        qtssTeardownMethod, 
        qtssPlayMethod, 
        qtssPauseMethod,
        qtssGetParameterMethod
    };
    QTSSModuleUtils::SetupSupportedMethods(inParams->inServer, sSupportedMethods, 6);

    return QTSS_NoErr;
}

Bool16 isTS(QTSS_StandardRTSP_Params* inParamBlock)
{
    Bool16 tsSuffix = false;

    char* path = NULL;
    UInt32 len = 0;
    QTSS_LockObject(inParamBlock->inRTSPRequest);
    QTSS_Error theErr = QTSS_GetValuePtr(inParamBlock->inRTSPRequest, qtssRTSPReqLocalPath, 0, (void**)&path, &len);
    Assert(theErr == QTSS_NoErr);

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: The file is %s", path);

    if (sTSSuffix.Len <= len) {
        if (path[len-1] == '/') {
            StrPtrLen thePath (&path[len - sTSSuffix.Len - 1], sTSSuffix.Len);
            tsSuffix = thePath.Equal (sTSSuffix);
        } else {
            StrPtrLen thePath (&path[len - sTSSuffix.Len], sTSSuffix.Len);
            tsSuffix = thePath.Equal (sTSSuffix);
        }
    }

    QTSS_UnlockObject(inParamBlock->inRTSPRequest);

    return tsSuffix;
}

QTSS_Error ProcessRTSPRequest(QTSS_StandardRTSP_Params* inParamBlock)
{
    QTSS_RTSPMethod* theMethod = NULL;
    UInt32 theMethodLen = 0;
    if ((QTSS_GetValuePtr(inParamBlock->inRTSPRequest,
                          qtssRTSPReqMethod,
                          0,
                          (void**)&theMethod,
                          &theMethodLen) != QTSS_NoErr) ||
        (theMethodLen != sizeof(QTSS_RTSPMethod))) {
        return QTSS_RequestFailed;
    }

    if (!isTS (inParamBlock)) { // We don't process files that's not ts.
        return QTSS_NoErr;
    }

    QTSS_Error err = QTSS_NoErr;
    switch (*theMethod) {
    case qtssDescribeMethod:
        err = DoDescribe(inParamBlock);
        break;
    case qtssSetupMethod:
        err = DoSetup(inParamBlock);
        break;
    case qtssPlayMethod:
        err = DoPlay(inParamBlock);
        break;
    case qtssTeardownMethod:
        (void)QTSS_Teardown(inParamBlock->inClientSession);
        (void)QTSS_SendStandardRTSPResponse(inParamBlock->inRTSPRequest, inParamBlock->inClientSession, 0);
        break;
    case qtssPauseMethod:
        err = DoPause (inParamBlock);
        break;
    case qtssGetParameterMethod:
        err = DoGetParameterMethod (inParamBlock);
        break;
    default:
        break;
    }
    if (err != QTSS_NoErr)
        (void)QTSS_Teardown(inParamBlock->inClientSession);

    return QTSS_NoErr;
}

QTSS_Error DoDescribe (QTSS_StandardRTSP_Params* inParamBlock)
{
    QTSS_Error theReturn;
    ResizeableStringFormatter theFullSDPBuffer (NULL,0);
    TSFileSession *theTSFileSession = NULL;
    static UInt16 BufSize = 200;
    QTSSCharArrayDeleter tempBuf (NEW char[BufSize]);

    UInt32 thePathLen = 0;
    OSCharArrayDeleter thePath(QTSSModuleUtils::GetFullPath(inParamBlock->inRTSPRequest,
                                                            qtssRTSPReqFilePath,
                                                            &thePathLen));
    StrPtrLen requestPath(thePath.GetObject(), ::strlen(thePath.GetObject()));

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: DoDescribe, Moviepath:%s", requestPath.Ptr);

    theTSFileSession = NEW TSFileSession (requestPath.Ptr);
    theReturn = theTSFileSession->Initialize ();

    if (theReturn == QTSS_NoErr) { // Store this newly created file object in the client session.
        QTSS_Error theErr;
        theErr = QTSS_SetValue(inParamBlock->inClientSession,
                               sTSFileSessionAttr,
                               0,
                               &theTSFileSession,
                               sizeof(theTSFileSession));
    } else if (theReturn == QTSS_FileNotFound ) {
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: file not found");
        return theReturn;
    } else if (theReturn == QTSS_BadIndex) { //Response invalide Index file
        StrPtrLen pathStr;
        QTSS_Error theErr;
        (void)QTSS_LockObject (inParamBlock->inRTSPRequest);
        (void)QTSS_GetValuePtr (inParamBlock->inRTSPRequest, qtssRTSPReqFilePath, 0, (void**)&pathStr.Ptr, &pathStr.Len);
        theErr = QTSSModuleUtils::SendErrorResponse (inParamBlock->inRTSPRequest,
                                                     qtssClientNotFound,
                                                     sNoIndexFileFoundErr,
                                                     &pathStr);
        (void)QTSS_UnlockObject (inParamBlock->inRTSPRequest);
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: invalid index file");
        return theErr;
    } else { // other err
        QTSS_Log (qtssWarningVerbosity, "[TSFileModule]: unknown error");
        return theReturn;
    }

    ////////////// SDP Session Description ///////////////
    // -------- version 
    theFullSDPBuffer.Put (sSDPVersionHeader);
    theFullSDPBuffer.Put (sEOL);

    // -------- session
    char* fileNameStr = NULL;
    (void)QTSS_GetValueAsString (inParamBlock->inRTSPRequest, qtssRTSPReqFilePath, 0, &fileNameStr);
    QTSSCharArrayDeleter fileNameStrDeleter (fileNameStr);
    theFullSDPBuffer.Put (sSDPSessionNameHeader);
    theFullSDPBuffer.Put (fileNameStr);
    theFullSDPBuffer.Put (sEOL);

    // -------- connection information header
    theFullSDPBuffer.Put(sConnectionHeader); 
    theFullSDPBuffer.Put(sEOL);

    // -------- time header t=0 0 is a permanent always available movie.
    theFullSDPBuffer.Put(sPermanentTimeHeader);
    theFullSDPBuffer.Put(sEOL);

    // -------- range
    theFullSDPBuffer.Put (sSDPAttrRangeHeader);
    qtss_sprintf (tempBuf, "%.3f-%.3f", // second
                  (double)0.,
                  (double)theTSFileSession->GetDuration () / 1000);
    theFullSDPBuffer.Put (tempBuf);
    theFullSDPBuffer.Put (sEOL);

    /////////// SDP Media Description ///////////////
    // -------- media
    theFullSDPBuffer.Put (sSDPMediaNameHeader);
    theFullSDPBuffer.Put (sEOL);

    // -------- rtpmap
    theFullSDPBuffer.Put (sSDPRTPMAPAttrib);
    theFullSDPBuffer.Put (sEOL);

    // -------- control
    theFullSDPBuffer.Put (sSDPControlHeader);
    theFullSDPBuffer.Put (sEOL);

    // -------- formate specific parameter
    qtss_sprintf (tempBuf, " index-file-version=%s;video-type=%d;frame-rate=%f;audio-type:%d", 
                  theTSFileSession->fIndex.GetVersion (),
                  theTSFileSession->fIndex.GetVideoType (),
                  theTSFileSession->fIndex.GetFrameRate (),
                  theTSFileSession->fIndex.GetAudioType ());
    theFullSDPBuffer.Put (sSDPAttrFormateParaHeader);
    theFullSDPBuffer.Put (tempBuf);
    theFullSDPBuffer.Put (sEOL);

    qtss_sprintf(tempBuf, "%"_S32BITARG_"", theFullSDPBuffer.GetBytesWritten());
    (void)QTSS_AppendRTSPHeader(inParamBlock->inRTSPRequest, qtssContentLengthHeader, tempBuf, ::strlen(tempBuf));

    (void)QTSS_SendStandardRTSPResponse(inParamBlock->inRTSPRequest, inParamBlock->inClientSession, 0);
    (void)QTSS_Write (inParamBlock->inRTSPRequest,
                      theFullSDPBuffer.GetBufPtr(),
                      theFullSDPBuffer.GetBytesWritten(),
                      NULL,
                      0);

    theFullSDPBuffer.PutTerminator ();
    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: sdp:\n%s", theFullSDPBuffer.GetBufPtr ());

    return QTSS_NoErr;
}

QTSS_Error DoSetup (QTSS_StandardRTSP_Params* inParamBlock)
{
    QTSS_Error theReturn;
    UInt32 theLen = 0;
    TSFileSession* theTSFileSession = NULL;

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: DoSetup");

    theLen = sizeof(TSFileSession*);
    theReturn = QTSS_GetValue (inParamBlock->inClientSession, sTSFileSessionAttr, 0, (void*)&theTSFileSession, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(TSFileSession*)) || (theTSFileSession == NULL))
        return QTSS_RequestFailed;

    StrPtrLen theTransport;
    theReturn = QTSS_GetValuePtr (inParamBlock->inRTSPHeaders, qtssTransportHeader, 0, (void **)&theTransport.Ptr, &theTransport.Len);
    if (theReturn != QTSS_NoErr)
        return QTSS_RequestFailed;

    if (NULL != theTransport.FindStringIgnoreCase ((char *)"RTP/AVP")) {
        //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: transport type %s\n", "rtp/avp");
        theTSFileSession->fTransportType = RTPAVPTransport;
    } else if (NULL != theTransport.FindStringIgnoreCase ((char *)"RAW/RAW/UDP")) {
        QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: transport type %s\n", "raw udp");
        theTSFileSession->fTransportType = RAWUDPTransport;
    } else if (NULL != theTransport.FindStringIgnoreCase ((char *)"MP2T/DVBC/QAM")) {
        QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: transport type %s\n", "ipqam");
        theTSFileSession->fTransportType = IPQAMTransport;
    } else {
        QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: invalid transport\n");
        return QTSS_RequestFailed;
    }

    //Create a new RTP stream           
    QTSS_RTPStreamObject newStream = NULL;
    theReturn = QTSS_AddRTPStream (inParamBlock->inClientSession, inParamBlock->inRTSPRequest, &newStream, 0);
    if (theReturn != QTSS_NoErr) {
        QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: AddRTPStream failure:%d\n", theReturn);
        return theReturn;
    }

    // Set the number of quality levels. Allow up to 6
    static UInt32 sNumQualityLevels = 6;
    theReturn = QTSS_SetValue (newStream, qtssRTPStrNumQualityLevels, 0, &sNumQualityLevels, sizeof(sNumQualityLevels));
    Assert(theReturn == QTSS_NoErr);

    theReturn = QTSS_SendStandardRTSPResponse (inParamBlock->inRTSPRequest, newStream, 0);
    Assert(theReturn == QTSS_NoErr);

    // UDP, theTSFileSession->fSendPacketPtr=&fReadBuffer[12], discard RTP header
    // RTP, theTSFileSession->fSendPacketPtr=&fReadBuffer[0]

    /************* rtp header ****************
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |V=2|P|X|  CC   |M|     PT      |       sequence number         |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                           timestamp                           |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |           synchronization source (SSRC) identifier            |
    +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    |            contributing source (CSRC) identifiers             |
    |                             ....                              |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    */
    UInt32 theSSRC = 0;
    SInt16 firstSeqNum = 0;
    SInt32 firstTimestamp = 0;
    switch (theTSFileSession->fTransportType) {
    case RTPAVPTransport : // for rtp
        theTSFileSession->fSendPacketPtr = &(theTSFileSession->fReadBuffer[0]); //include RTP header
        theTSFileSession->fReadBuffer[0] = 0x80; // v=2
        theTSFileSession->fReadBuffer[1] = 0x21; // PT=33
        theTSFileSession->fFirstSeqNum = firstSeqNum = ::rand ();
        theTSFileSession->fReadBuffer[2] = (unsigned char)(firstSeqNum >> 8);
        theTSFileSession->fReadBuffer[3] = (unsigned char)firstSeqNum;
        theTSFileSession->fFirstTimestamp = firstTimestamp = ::rand ();
        theTSFileSession->fReadBuffer[4] = (unsigned char)(firstTimestamp >> 24);
        theTSFileSession->fReadBuffer[5] = (unsigned char)(firstTimestamp >> 16);
        theTSFileSession->fReadBuffer[6] = (unsigned char)(firstTimestamp >> 8);
        theTSFileSession->fReadBuffer[7] = (unsigned char)firstTimestamp;
        theReturn = QTSS_GetValue (newStream, qtssRTPStrSSRC, 0, (void*)&theSSRC, &theLen);
        theTSFileSession->fReadBuffer[8] = (unsigned char)(theSSRC >> 24);
        theTSFileSession->fReadBuffer[9] = (unsigned char)(theSSRC >> 16);
        theTSFileSession->fReadBuffer[10] = (unsigned char)(theSSRC >> 8);
        theTSFileSession->fReadBuffer[11] = (unsigned char)theSSRC;
        theTSFileSession->fTransportPayloadSize = 1328;
        break;
    case RAWUDPTransport : // raw udp
        theTSFileSession->fSendPacketPtr = &(theTSFileSession->fReadBuffer[12]); // without RTP header, 12 bytes
        theTSFileSession->fTransportPayloadSize = 1316;
        break;
    case IPQAMTransport :
        theTSFileSession->fSendPacketPtr = &(theTSFileSession->fReadBuffer[12]); // without RTP header, 12 bytes
        theTSFileSession->fTransportPayloadSize = 1316;
    }

    return QTSS_NoErr;
}

QTSS_Error DoPlay (QTSS_StandardRTSP_Params* inParamBlock)
{
    QTSS_Error theReturn;
    UInt32 theLen = 0;
    TSFileSession* theTSFileSession = NULL;
    QTSS_RTPStreamObject* theStream = NULL;

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: DoPlay");

    theLen = sizeof(TSFileSession*);
    theReturn = QTSS_GetValue (inParamBlock->inClientSession, sTSFileSessionAttr, 0, (void*)&theTSFileSession, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(TSFileSession*)) || (theTSFileSession == NULL))
        return QTSS_RequestFailed;

    Float64* theStartTimeP = NULL;
    theReturn = QTSS_GetValuePtr (inParamBlock->inRTSPRequest, qtssRTSPReqStartTime, 0, (void**)&theStartTimeP, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(Float64))) {
        theTSFileSession->SetLastPlayStartTime ((float)theTSFileSession->fCurrentPlayTime);
    } else {
        SInt64 theRealPosition = theTSFileSession->Seek ((UInt64)*theStartTimeP * 1000);
        if (theTSFileSession->fEOS == true) {
            qtss_printf("---------------the end\n"); //TODO
        }
        theTSFileSession->SetLastPlayStartTime(theRealPosition); // ms
    }

    Float64* theStopTimeP = NULL;
    theReturn = QTSS_GetValuePtr (inParamBlock->inRTSPRequest, qtssRTSPReqStopTime, 0, (void**)&theStopTimeP, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(Float64)) || (theStopTimeP == NULL)) {
        theTSFileSession->SetLastPlayStopTime ((float)theTSFileSession->GetDuration()/1000);
    } else {
        if (*theStopTimeP != -1) {
            theTSFileSession->SetLastPlayStopTime (*theStopTimeP);
        } else {
            theTSFileSession->SetLastPlayStopTime ((float)theTSFileSession->GetDuration()/1000); // ms
        }
    }

    theReturn = QTSS_GetValuePtr (inParamBlock->inClientSession, qtssCliSesStreamObjects, 0, (void**)&theStream, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(theStream)) || (theStream == NULL))
        return QTSS_RequestFailed;

    if (theTSFileSession->fPaused == true) { // ajust fTotalPauseTime
        if (theTSFileSession->fTransportType == RTPAVPTransport) {
            UInt16 firstSeqNum;
            UInt32 firstTimestamp;
            if (theTSFileSession->fLastPauseTime == 0) { // first play
                firstSeqNum = theTSFileSession->fFirstSeqNum + 1;
                firstTimestamp = theTSFileSession->fFirstTimestamp;
            } else { // play after pause
                unsigned char* theRTPHeader = (unsigned char *)(theTSFileSession->fSendPacketPtr);

                // sequence
                ((unsigned char *)&firstSeqNum)[0] = theRTPHeader[3];
                ((unsigned char *)&firstSeqNum)[1] = theRTPHeader[2];

                if (theStartTimeP != NULL) { // seek play, that means range header
                    theTSFileSession->fPacket.packetData = NULL;
                    firstSeqNum ++;
                }

                // timestamp
                ((unsigned char *)&firstTimestamp)[3] = theRTPHeader[4];
                ((unsigned char *)&firstTimestamp)[2] = theRTPHeader[5];
                ((unsigned char *)&firstTimestamp)[1] = theRTPHeader[6];
                ((unsigned char *)&firstTimestamp)[0] = theRTPHeader[7];
                firstTimestamp += (QTSS_Milliseconds () - theTSFileSession->fLastPauseTime) * 90;
                theTSFileSession->fFirstTimestamp = firstTimestamp;
                theRTPHeader[4] = ((unsigned char *)&firstTimestamp)[3];
                theRTPHeader[5] = ((unsigned char *)&firstTimestamp)[2];
                theRTPHeader[6] = ((unsigned char *)&firstTimestamp)[1];
                theRTPHeader[7] = ((unsigned char *)&firstTimestamp)[0];
            }

            theReturn = QTSS_SetValue (*theStream,
                                       qtssRTPStrFirstSeqNumber,
                                       0,
                                       &firstSeqNum,
                                       sizeof(firstSeqNum));
        
            theReturn = QTSS_SetValue (*theStream,
                                       qtssRTPStrFirstTimestamp,
                                       0,
                                       &firstTimestamp,
                                       sizeof(firstTimestamp));
        }
        // add the range header.
        char rangeHeader[64];
        qtss_snprintf(rangeHeader, sizeof(rangeHeader) - 1, "npt=%.3f-%.3f",
                      theTSFileSession->GetLastPlayStartTime () / 1000,
                      theTSFileSession->GetLastPlayStopTime ());
        rangeHeader[sizeof(rangeHeader) -1] = 0;
        StrPtrLen rangeHeaderPtr(rangeHeader);
        (void)QTSS_AppendRTSPHeader(inParamBlock->inRTSPRequest, qtssRangeHeader, rangeHeaderPtr.Ptr, rangeHeaderPtr.Len);

        theTSFileSession->fPaused = false;
    } else { //TODO seek play without pause like above
        //make sure to clear the next packet that server have sent!
        theTSFileSession->fPacket.packetData = NULL;
    }

    switch (theTSFileSession->fTransportType) {
    case RTPAVPTransport : // for rtp
        (void)QTSS_SendStandardRTSPResponse (inParamBlock->inRTSPRequest,
                                             inParamBlock->inClientSession,
                                             qtssPlayRespWriteTrackInfo);
        theReturn = QTSS_Play (inParamBlock->inClientSession, inParamBlock->inRTSPRequest, qtssPlayFlagsSendRTCP);
        break;
    case RAWUDPTransport : // for udp
    case IPQAMTransport : // for ipqam
        (void)QTSS_SendStandardRTSPResponse (inParamBlock->inRTSPRequest, inParamBlock->inClientSession, 0);
        theReturn = QTSS_Play (inParamBlock->inClientSession, inParamBlock->inRTSPRequest, 0);
    }

    if (theReturn != QTSS_NoErr)
        return theReturn;

    return QTSS_NoErr;
}

QTSS_Error DoPause (QTSS_StandardRTSP_Params* inParamBlock)
{
    QTSS_Error theReturn;
    UInt32 theLen = 0;
    TSFileSession* theTSFileSession = NULL;
    QTSS_RTPStreamObject* theStream = NULL;
    QTSS_TimeVal theNow = QTSS_Milliseconds ();

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: DoPause");

    theLen = sizeof(TSFileSession*);
    theReturn = QTSS_GetValue (inParamBlock->inClientSession, sTSFileSessionAttr, 0, (void*)&theTSFileSession, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(TSFileSession*)) || (theTSFileSession == NULL))
        return QTSS_RequestFailed;

    theReturn = QTSS_GetValue (inParamBlock->inClientSession, qtssCliSesStreamObjects, 0, (void *)&theStream, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(theStream)) || (theStream == NULL))
        return QTSS_RequestFailed;

    // Flush socket
    QTSS_Flush (theStream);

    theTSFileSession->fPaused = true;
    theTSFileSession->fLastPauseTime = theNow;

    (void)QTSS_Pause(inParamBlock->inClientSession);
    (void)QTSS_SendStandardRTSPResponse(inParamBlock->inRTSPRequest, inParamBlock->inClientSession, 0);

    return QTSS_NoErr;
}

QTSS_Error DoGetParameterMethod(QTSS_StandardRTSP_Params* inParamBlock)
{
    QTSS_Error theReturn;
    UInt32 theLen = 0;
    TSFileSession* theTSFileSession = NULL;
    UInt64 theCurrentPlayTime = 0;
    QTSSCharArrayDeleter tempBuf (NEW char[50]);

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: DoGetParameter");

    theLen = sizeof(TSFileSession*);
    theReturn = QTSS_GetValue (inParamBlock->inClientSession, sTSFileSessionAttr, 0, (void*)&theTSFileSession, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(TSFileSession*)) || (theTSFileSession == NULL))
        return QTSS_RequestFailed;

    theCurrentPlayTime = theTSFileSession->GetCurrentPlayTime ();
    qtss_sprintf (tempBuf, "%lld", theCurrentPlayTime); // ??, without \0

    (void)QTSS_AppendRTSPHeader(inParamBlock->inRTSPRequest, qtssTSTimeinfoHeader, tempBuf, strlen(tempBuf));
    (void)QTSS_SendStandardRTSPResponse(inParamBlock->inRTSPRequest, inParamBlock->inClientSession, 0);

    return QTSS_NoErr;
}

QTSS_Error SendPackets(QTSS_RTPSendPackets_Params* inParams)
{
    QTSS_Error theReturn;
    UInt32 theTransportType = qtssWriteFlagsNoFlags;

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: SendPackets");

    TSFileSession* theTSFileSession = NULL;
    UInt32 theLen = sizeof(TSFileSession*);
    theReturn = QTSS_GetValue (inParams->inClientSession, sTSFileSessionAttr, 0, (void*)&theTSFileSession, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(TSFileSession*)) || (theTSFileSession == NULL))
        return QTSS_RequestFailed;

    if (theTSFileSession->GetLastPlayStartTime() == -1 || theTSFileSession->fPaused == true ) {//Not start or Paused
        inParams->outNextPacketTime = qtssDontCallSendPacketsAgain;    
        return QTSS_NoErr;
    }

    QTSS_RTPStreamObject theStream = NULL;
    theReturn = QTSS_GetValue (inParams->inClientSession, qtssCliSesStreamObjects, 0, (void *)&theStream, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(theStream)) || (theStream == NULL))
        return QTSS_RequestFailed;

    QTSS_TimeVal* theLastPlayTime = NULL;
    theReturn = QTSS_GetValuePtr (inParams->inClientSession,
                                  qtssCliSesPlayTimeInMsec,
                                  0,
                                  (void **)&theLastPlayTime,
                                  &theLen);
    if ((theReturn != QTSS_NoErr) || (theLastPlayTime == NULL))
        return QTSS_RequestFailed;

    switch (theTSFileSession->fTransportType) {
    case RTPAVPTransport : // for rtp
        theTransportType = qtssWriteFlagsIsRTP;
        break;
    case RAWUDPTransport : // for udp
    case IPQAMTransport : // for ipqam
        theTransportType = qtssWriteFlagsIsRawUDP;
    }

    SInt64 theTransmitTime;
    while (true) { // send packets loop
        if (theTSFileSession->fPacket.packetData == NULL) {
            theTransmitTime = theTSFileSession->GetNextPacket ();

            if ((true == theTSFileSession->fEOS) || (theTSFileSession->fNextPacketLen == 0)) {// end of file, should stop
                inParams->outNextPacketTime = qtssDontCallSendPacketsAgain;
                break;
            }

            theTSFileSession->fPacket.packetTransmitTime = *theLastPlayTime
                + (theTransmitTime - (SInt64)theTSFileSession->GetLastPlayStartTime ());

/*
            qtss_printf ("First:%lld;TransmitTime:%lld, packetTransmitTime:%lld;start:%.3f;durrent:%lld\n",
                         *theLastPlayTime,
                         theTransmitTime,
                         theTSFileSession->fPacket.packetTransmitTime,
                         theTSFileSession->GetLastPlayStartTime(),
                         theTSFileSession->fCurrentPlayTime);
*/
            if (theTSFileSession->fTransportType == RTPAVPTransport) { // for rtp
                // rtp header, sequence and timestamp
                UInt16 theSequence;
                unsigned char* theRTPHeader;
                theRTPHeader = (unsigned char *)(theTSFileSession->fPacket.packetData);
                ((unsigned char *)&theSequence)[0] = theRTPHeader[3];
                ((unsigned char *)&theSequence)[1] = theRTPHeader[2];
                theSequence += 1;
                //qtss_printf ("sequence:%d\n", theSequence);
                theRTPHeader[3] = ((unsigned char *)&theSequence)[0];
                theRTPHeader[2] = ((unsigned char *)&theSequence)[1];
                
                // timestamp
                UInt32 theRTPTimestamp;
                // clock rate: 90000 Hz
                theRTPTimestamp = theTSFileSession->fFirstTimestamp + (theTransmitTime - theTSFileSession->GetLastPlayStartTime ()) * 90;
                theRTPHeader[4] = ((unsigned char *)&theRTPTimestamp)[3];
                theRTPHeader[5] = ((unsigned char *)&theRTPTimestamp)[2];
                theRTPHeader[6] = ((unsigned char *)&theRTPTimestamp)[1];
                theRTPHeader[7] = ((unsigned char *)&theRTPTimestamp)[0];
            }
        }

        theReturn = QTSS_Write(theStream, &theTSFileSession->fPacket, theTSFileSession->fTransportPayloadSize, NULL, theTransportType);

        if (theReturn == QTSS_WouldBlock) {

            if (theTSFileSession->fPacket.suggestedWakeupTime == -1)
                inParams->outNextPacketTime = sFlowControlProbeInterval; // for buffering, try me again in # MSec
            else {
                Assert (theTSFileSession->fPacket.suggestedWakeupTime > inParams->inCurrentTime);
                inParams->outNextPacketTime = theTSFileSession->fPacket.suggestedWakeupTime - inParams->inCurrentTime;
            }

/*
            qtss_printf("suggest:%lld, current:%lld, Call again: %d\n",
                        theTSFileSession->fPacket.suggestedWakeupTime,
                        inParams->inCurrentTime,
                        inParams->outNextPacketTime);
*/
            return QTSS_NoErr;
        } else {
            theTSFileSession->fPacket.packetData = NULL;
        }
    }

    return QTSS_NoErr;
}

QTSS_Error DestroySession (QTSS_ClientSessionClosing_Params* inParams)
{
    TSFileSession** theTSFileSession = NULL;
    UInt32 theLen = 0;
    QTSS_Error theReturn;

    theReturn = QTSS_GetValuePtr (inParams->inClientSession, sTSFileSessionAttr, 0, (void **)&theTSFileSession, &theLen);
    if ((theReturn != QTSS_NoErr) || (theLen != sizeof(TSFileSession*)) || (theTSFileSession == NULL)) {
        QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: Not a TS file session");
        return QTSS_RequestFailed;
    }

    //QTSS_Log (qtssMessageVerbosity, "[TSFileModule]: DestroySession");
    delete *theTSFileSession;

    return QTSS_NoErr;
}

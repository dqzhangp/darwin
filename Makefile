# Copyright (c) 1999 Apple Computer, Inc.  All rights reserved.
#  
ROOTDIR=$(shell pwd)
CPLUS=g++
CCOMP=gcc
LINKER=g++
AR=ar
RANLIB=ranlib
MAKE=make
COMPILER_FLAGS=-Wall -fPIC -D_REENTRANT -D__USE_POSIX -D__linux__ -pipe -I../server
#INCLUDE_FLAG=-include
CORE_LINK_LIBS=-lpthread -ldl -lstdc++ -lm -lcrypt
SHARED=-shared

CCFLAGS += $(COMPILER_FLAGS) -DDSS_USE_API_CALLBACKS -g -Wall
# OPTIMIZATION
CCFLAGS += -O3
# EACH DIRECTORY WITH HEADERS MUST BE APPENDED IN THIS MANNER TO THE CCFLAGS
CCFLAGS += -I$(ROOTDIR)
CCFLAGS += -I$(ROOTDIR)/modules
CCFLAGS += -I$(ROOTDIR)/modules/QTSSFileModule
CCFLAGS += -I$(ROOTDIR)/modules/TSFileModule
CCFLAGS += -I$(ROOTDIR)/lib
CCFLAGS += -I$(ROOTDIR)/server

# EACH DIRECTORY WITH A STATIC LIBRARY MUST BE APPENDED IN THIS MANNER TO THE LINKOPTS
LINKOPTS = -Wl,-rpath,$(ROOTDIR)/lib
LINKOPTS += -L$(ROOTDIR)/lib -lts3

export CPLUS
export CCOMP
export LINKER
export AR
export RANLIB
export COMPILER_FLAGS
export INCLUDE_FLAG
export CORE_LINK_LIBS
export SHARED
export MODULE_LIBS
export CCFLAGS
export LINKOPTS

QTSSFILEMODULE = QTSSFileModule
TSFILEMODULE = TSFileModule

LIBFILES += lib/libts3.so

NAME=darwin

all: $(NAME) $(QTSSFILEMODULE) $(TSFILEMODULE) 

$(LIBFILES):
	$(MAKE) -C lib

$(QTSSFILEMODULE): $(LIBFILES)
	$(MAKE) -C modules/QTSSFileModule

$(NAME): $(LIBFILES)
	$(MAKE) -C server

$(TSFILEMODULE):
	$(MAKE) -C modules/TSFileModule

clean:
	$(MAKE) -C lib clean
	$(MAKE) -C modules/QTSSFileModule clean
	$(MAKE) -C modules/TSFileModule clean
	$(MAKE) -C server clean

.SUFFIXES: .cpp .c .o

.cpp.o:
	$(CPLUS) -c -o $*.o $(DEFINES) $(CCFLAGS) $*.cpp

.c.o:
	$(CCOMP) -c -o $*.o $(DEFINES) $(CCFLAGS) $*.c

.PHONY: $(LIBFILES)
